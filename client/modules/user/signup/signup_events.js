if (Meteor.isClient) {
  // counter starts at 0
	Template.signUp.events({
	'click #signIn' : function(event, t){
	  event.preventDefault();
	  Router.go('signIn');    
	},

	'submit #signup-form_id' : function(event, template) {
	  	event.preventDefault();
	  	//$(".alert").remove();

	  	var email = validateEmail(template.find('#email_id').value);
	  	var userPassword = validatePassword(template.find('#password_id'));
	  	var username = isNotEmpty(template.find('#name_id').value);
	  	var fullname = isNotEmpty(template.find('#username_id').value);

	  
	  	if ( userPassword &&  email && username && fullname === true ){// &amp;&amp; other validations) {
			var data =  {
		      				email:template.find('#email_id').value,
		      				password:template.find('#password_id').value,
		      				profile:{
		      					fullname:template.find('#name_id').value,
		      					username:template.find('#username_id').value,
		      				}
		      			};

		    //console.log("data " + JSON.stringify(data))
		    Meteor.call('createUserWithRole',data, function(err, res){		
			    if(res == 1){

			    	Bert.alert( 'Registrado, revisa tu correo electrónico', 'success' );
			      	
			      	/*var liData = '<h4 id="alert" align="center" class="alert" style="color:green;display:none;">Registrado, revisa tu correo electrónico</h4>';
      			  	$(liData).appendTo('.form-actions').fadeIn('slow');*/

			    }else if (res == 3){

			    	Bert.alert( "Esta dirección de email ya existe", 'danger' );

			       	/*var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Esta dirección de email ya existe</h4>';
      			  	$(liData).appendTo('.form-actions').fadeIn('slow');*/

			    }else{
			    	Bert.alert( "Problemas, Intenta de nuevo " + err, 'danger' );
			    	
			    	/*var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;" align="center">Problemas, Intenta de nuevo</h4>';
      			  	$(liData).appendTo('.form-actions').slideDown();*/
			    }
			});
			// Then use the Meteor.createUser() function*/
		}else{
			Bert.alert( "Rellena todos los campos correctamente", 'danger' );
		}
	
	  	return false;
	}

		

	});

	Template.signUp.rendered = function(){
		$("body").addClass("page-signup");
		
	}


	validateEmail = function ( email ) {
		expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if ( !expr.test(email) )
		    alert("Error: La dirección de correo " + email + " es incorrecta.");
		else{
			return true
		}
	}

	validatePassword = function (fld) {
	    var error = "";
	    var illegalChars = /[\W_]/; // allow only letters and numbers
	 
	    if (fld.value == "") {
	        fld.style.background = 'Yellow';
	        error = "You didn't enter a password.\n";
	        alert(error);
	        return false;
	 
	    } else if ((fld.value.length < 7) || (fld.value.length > 15)) {
	        error = "The password is the wrong length. \n";
	        fld.style.background = 'Yellow';
	        alert(error);
	        return false;
	 
	    } else if (illegalChars.test(fld.value)) {
	        error = "The password contains illegal characters.\n";
	        fld.style.background = 'Yellow';
	        alert(error);
	        return false;
	 
	    } else if ( (fld.value.search(/[a-zA-Z]+/)==-1) || (fld.value.search(/[0-9]+/)==-1) ) {
	        error = "The password must contain at least one numeral.\n";
	        fld.style.background = 'Yellow';
	        alert(error);
	        return false;
	 
	    } else {
	        fld.style.background = 'White';
	    }
	   return true;
	}

	isNotEmpty = function(value) {
	    if (value && value !== ''){
	        return true;
	    }
	    //console.log('Please fill in all required fields.');
	    return false;
	};


}


