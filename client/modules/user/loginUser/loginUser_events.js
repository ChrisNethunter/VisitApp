if (Meteor.isClient) {
  // counter starts at 0
  Template.loginUser.events({
  	'submit #signin-form_id' : function(event, t){
      	event.preventDefault();
        //$(".alert").remove();
      	// retrieve the input field values
      	var email = t.find('#username_id').value
        , password = t.find('#password_id').value;

        if (isNotEmpty(email , password) === true ){
          // Trim and validate your fields here.... 
          // If validation passes, supply the appropriate fields to the
          // Meteor.loginWithPassword() function.
          Meteor.loginWithPassword(email, password, function(err,res){
            if (err){
              if (err.message === 'User not found [403]') {

                Bert.alert( "Usuario no encontrado", 'danger' );
                /*var liData = '<h4 id="alert" align="center" class="alert" style="color:red;display:none;">Usuario no encontrado</h4>';
                $(liData).appendTo('.form-actions').fadeIn('slow');*/
              }else{
                Bert.alert( "Datos incorrectos", 'danger' );
              }
            }else{
              Router.go('dashboard');
              // The user has been logged in.
              // The user might not have been found, or their passwword
              // could be incorrect. Inform the user that their
              // login attempt has failed. 
            } 
          });
        }else{
          Bert.alert( "Formulario vacio", 'danger' );
        };
        
         return false; 
    },

    'click #singup' : function(event, t){
      event.preventDefault();
      Router.go('singUp');    
    },
    
    'click #forgot-password-link' : function(event, t){
        $('#password-reset-form').fadeIn(400);
        return false;
    },

    'click #password-reset-form .close' : function(event, t){
        $('#password-reset-form').fadeOut(400);
        return false;
    },

    'submit #password-reset-form_id': function(e, t) {
        $(".alert").remove();
        e.preventDefault();

        var forgotPasswordForm = $(e.currentTarget),
            email = trimInput(forgotPasswordForm.find('#p_email_id').val());

        if (isNotEmpty(email) && validateEmail(email)) {

            Accounts.forgotPassword({email: email}, function(err) {
              if (err) {
                if (err.message === 'User not found [403]') {

                  Bert.alert( "Este correo no existe", 'danger' );

                } else {
                  Bert.alert( "Lo sentimos ha ocurrido un error. " + err , 'danger' );
                }
              } else {
                Bert.alert( 'Revisa tu correo electrónico', 'success' ); 
              }
            });

        }
        return false;

    },
    
    /*New Password*/

    'submit #password-new-form': function(e, t) {
        e.preventDefault();
        $(".alert").remove();

        var password = t.find('#reset-password-new-password');

        if (isNotEmpty(password) && validatePassword(password)  === true) {

          var password = $('#reset-password-new-password').val();

          Accounts.resetPassword(Session.get('resetPassword'), password, function(err) {
            if (err) {
              Bert.alert( "Lo sentimos ha ocurrido un error. " + err , 'danger' );
            } else {
              Bert.alert( 'Tu contraseña ha sido cambiada. ¡Bienvenido de Vuelta!', 'success' ); 
              Session.set('resetPassword', null);
            }
          });
        }
        return false;
    },
    'click .close' : function(event, t){
        event.preventDefault();
        Router.go('signIn');    
    },
    /*New Password*/
    'click #facebook-login': function(event) {
        Meteor.loginWithFacebook({}, function(err){
            if (err) {
                throw new Meteor.Error("Facebook login failed");
            }
        });
    },

    'click #twitter-login': function(event) {
        Meteor.loginWithTwitter({}, function(err){
            if (err) {
                throw new Meteor.Error("Twitter login failed");
            }
        });
    },

    
  });

  Template.loginUser.rendered = function(){
  
  }

  trimInput = function(value) {
      return value.replace(/^\s*|\s*$/g, '');
  };


  if (Accounts._resetPasswordToken) {
    Session.set('resetPassword', Accounts._resetPasswordToken);
  }
}


