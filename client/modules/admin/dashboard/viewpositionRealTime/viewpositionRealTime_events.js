var map, marker, myLatlng;
Template.viewPositionRealTime.events({
	
  	'click #vs-reload': function(event){
        event.preventDefault();
        location.reload();
    },
});


Template.viewPositionRealTime.rendered = function(){

	setTimeout(function(){ 
		var url =  location.protocol + '//' + location.host;		
		initialize()
		//initMap()
		setInterval(function(){ 
			
			Meteor.call('getPositionReal',$("#seller").val(), function(err, res){
		      	if (!err){
		      		if (res){
		      			//console.log("data " + JSON.stringify(res));
				       	if (!myLatlng || !myLatlng.lng) {
					      myLatlng = new google.maps.LatLng(res.marker.lat, res.marker.lng);
					    } 

						var icon = {
						    url: url + "/img/marker.png", // url
						    scaledSize: new google.maps.Size(30, 30), // scaled size
						    origin: new google.maps.Point(0,0), // origin
						    anchor: new google.maps.Point(0, 0) // anchor
						};


					    if (!marker || !marker.setPosition) {
					        marker = new google.maps.Marker({
					            position: myLatlng,
					            title: $("#seller-data-detail").text(),
					            icon:icon ,
					        });
					        map.setZoom(15);
					        // To add the marker to the map, call setMap();
					        marker.setMap(map);
					    } else {
					        marker.setPosition(myLatlng);
					    }
		      		}
			    }
		    });
		}, 5000);
	}, 1000);
	
	
}

function initialize() {
    var mapOptions = {
        center: {
            lat: 4.814631,
            lng: -75.692930
        },
        zoom: 15
    };

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

}

function initMap  () {
    var uluru = {lat:4.814631, lng: -75.692930};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: uluru,
      apTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControlOptions: {
	        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
	  }
    });    
}

function refreshMap (myLatlng){
	
	if (!marker) {
        marker = new google.maps.Marker({
            position: myLatlng,
            title: "Hello World!"
        });
        // To add the marker to the map, call setMap();
        marker.setMap(map);
    } else {
        marker.setPosition(myLatlng);
    }
    
}





