Template.rutes_seller.helpers({
  	showRutes: function(object) {
       return Rutes.find({user:Meteor.userId()});
    },
    rutesNumber: function(object) {
       return Rutes.find({user:Meteor.userId()}).count();
    },

    tableSettings : function () {
             return {
                        rowsPerPage: 10,
                        fields: [
                            { 
                                key: "createdAt", 
                                label: 'Fecha' 

                            },
                            { 
                                key: "rute", 
                                label: 'Ruta' 

                            },
                            { 
                                key: "seller", 
                                label: 'Vendedor',
                                fn: function (seller, object) {
                                    var data = Meteor.users.findOne({_id:seller})
                                    var html =  '<p> ' + data.profile.name + ' </p>';
                                    return new Spacebars.SafeString(html);
                                } 

                            },
                            { 
                                key: '_id', 
                                label: 'Opciones',
                                fn: function (_id, object) {
                                    var html =  '<button id="'+_id+'" class="btn btn-primary btn-flat edit-rute">Editar</button>';
                                    return new Spacebars.SafeString(html);
                                }

                            },
                            
                           
                        ],
       
            };
    }
});



