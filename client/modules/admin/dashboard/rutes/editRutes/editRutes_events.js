Template.editRute.events({
  'click #cancelar': function () {
    event.preventDefault();
    Router.go('dashboardSellerRutes');    
  },
  'submit #ruteForm': function(event, template) {
      event.preventDefault();

      var data =  {
                    rute: $("#rute").val(),
                    seller:$("#seller").val(),
                    clients :[],
                    modifyAt: dateString()["stringDate"], 
                    user:Meteor.userId(),
                  };


      $('#checkClients input:checked').each(function() {

          data.clients.push(  {
                        idClient:$(this).val(),
                        name:$(this).attr('name')
                      }
                    );
      });

      registerEditRoute(data);
  },
});

Template.editRute.onCreated(function () {
  Meteor.subscribe("sellers");
  Meteor.subscribe("allClients");
});

Template.editRute.rendered = function(){
  setTimeout(function(){ 
    Meteor.subscribe("rutes")
    checkEdit()
  }, 500);
}

function checkEdit(){
  var rutes = Rutes.findOne({_id:$("#ruteId").val()});
  $
  for (var i=0; i < Object.keys(rutes.clients).length; i++){
      $("#" + rutes.clients[i].idClient ).prop( "checked", true );
  }
  //console.log("rutes " + JSON.stringify(rutes))
}

function registerEditRoute(data){

  if (isNotEmpty(data.rute) && isNotEmpty(data.seller) === true && data.clients.length > 0 ){


    Meteor.call('ruteEdit',data,$("#ruteId").val(), function(err, res){
      if (!err){
        sweetAlert("Buen Trabajo!", "Se ha editado la ruta correctamente!", "success");
        Router.go('dashboardSellerRutes');
      }else{
         sweetAlert('Oops...', 'Intenta de Nuevo ! ' + err, 'error');
      };
      
    });
  }else{
    sweetAlert('Oops...', 'Rellena todos los Campos Correctamente!', 'error');
  };
}




