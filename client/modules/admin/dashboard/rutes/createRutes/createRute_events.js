Template.createRute.events({
  'click #cancelar': function () {
    event.preventDefault();
    Router.go('dashboardSellerRutes');    
  },
  'submit #ruteForm': function(event, template) {
      event.preventDefault();

      var data =  {
                    rute: $("#rute").val(),
                    seller:$("#seller").val(),
                    clients :[],
                    createdAt: dateString()["stringDate"], 
                    date: new Date(),
                    user:Meteor.userId(),
                    status:false,
                  };


      $('#checkClients input:checked').each(function() {

          data.clients.push(  {
                        idClient:$(this).val(),
                        name:$(this).attr('name'),
                        status:"inactivo"
                      }
                    );
      });

      registerRoute(data);
  },
});

Template.createRute.onCreated(function () {
  Meteor.subscribe("sellers",Meteor.userId());
  Meteor.subscribe("clients",Meteor.userId());
});

Template.createRute.rendered = function(){
  
}

function registerRoute(data){

  if (isNotEmpty(data.rute) && isNotEmpty(data.seller) === true && data.clients.length > 0 ){


    Meteor.call('ruteInsert',data, function(err, res){
      if (!err){
        Bert.alert( 'Se ha registrado una nueva ruta!', 'success' ); 
        Router.go('dashboardSellerRutes');
      }else{
        Bert.alert( "'Intenta de Nuevo ! " + err, 'danger' );
      };
    });
  }else{
    Bert.alert( "Rellena todos los Campos Correctamente", 'danger' );
  };
}




