Template.rutes_seller.events({
  'click #register-rute' : function(event, t){
    event.preventDefault();
  	Router.go("register-rute")
  },
   'click .edit-rute': function(event){
        event.preventDefault();  
        Router.go('edit-rute',{_IdRute:event.currentTarget.id});
  },
	
});

Template.rutes_seller.onCreated(function () {
	Meteor.subscribe("rutes",Meteor.userId());
	Meteor.subscribe("sellers",Meteor.userId());
});

Template.rutes_seller.rendered = function(){

  
}


