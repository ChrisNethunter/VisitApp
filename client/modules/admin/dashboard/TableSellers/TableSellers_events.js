if (Meteor.isClient) {
  // counter starts at 0
  Template.TableSellers.events({
  	'click .update-seller': function (event) {
        event.preventDefault();
        //alert(event.currentTarget.id);
		    Router.go('updateSeller',{_IdSeller:event.currentTarget.id});    
    },

    'click .seller-gps': function (event) {
        event.preventDefault();
        //alert(event.currentTarget.id);
        Session.get("dataSell",event.currentTarget.id)
        Router.go('viewPositionReal',{_IdSeller:event.currentTarget.id});    
    },
  });

  Template.TableSellers.rendered = function(){

  }
}


