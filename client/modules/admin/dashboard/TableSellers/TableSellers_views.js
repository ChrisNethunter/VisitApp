if (Meteor.isClient) {
  	Template.TableSellers.helpers({
	    sellers: function(object) {
	    	return Meteor.users.find({"profile.enterprise":Meteor.userId()});
	    },
	    sellersNumber: function(object) {
	    	var data = Meteor.users.find({"profile.enterprise":Meteor.userId()}).count();
	    	return data;
	    },

	    tableSettings : function () {
	         return {
	                    rowsPerPage: 6,
	                    fields: [
	                        { 
	                            key: 'profile.cc', 
	                            label: 'Cédula' ,
	                        },
	                        { 
	                            key: 'emails.0.address',
	                            label: 'Email' 

	                        },
	                        { 
	                            key: "profile.name", 
	                            label: 'Nombre' 
	                        },
	                        { 
	                            key: "profile.phone", 
	                            label: 'Teléfono' 

	                        },
	                        { 
	                            key: "profile.status", 
	                            label: 'Estado',
	                            fn: function (estado, object) {
	                            	if (estado == "activo"){
	                            		var html =  '<div id="activo" class="btn btn-success btn-sm">Activo</div>';
	                            		
	                            	}else{
	                            		var html =  '<div id="inactivo" class="btn btn-danger btn-sm">Inactivo</div>';
	                            	};
	                            	return new Spacebars.SafeString(html);
	                                
	                            }

	                        },
	                        { 
	                            key: '_id', 
	                            label: 'Opciones',
	                            fn: function (_id, object) {
	                                var html =  '<button id="'+_id+'" class="update-seller btn btn-primary btn-flat">Modificar</button>';
	                                return new Spacebars.SafeString(html);
	                            }

	                        },
	                        { 
	                            key: '_id', 
	                            label: 'Mapa',
	                            fn: function (_id, object) {
	                                var html =  '<button id="'+_id+'" class="seller-gps btn btn-info btn-flat">GPS</button>';
	                                return new Spacebars.SafeString(html);
	                            }

	                        },
	                       
	                    ],
	   
	        };
	    }
	});
}


