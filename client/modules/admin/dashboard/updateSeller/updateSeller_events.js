if (Meteor.isClient) {
  Template.updateSeller.events({
    'click #cancelar': function () {
      event.preventDefault();
      Router.go('dashboard');    
    },

    'click .switcher-inner': function(event, template) {
      if ($('#status-seller').val()=="activo"){
        $('#status-seller').val("inactivo");
        $(".switcher-state-on").css("margin-left" , "-100%");
        $(".switcher-toggler").css("margin-left" , "1px");
      }else{
        $('#status-seller').val("activo");
        $(".switcher-state-on").css("margin-left" , "10%");
        $(".switcher-toggler").css("margin-left" , "39px");
      };
    },
    'submit #reisterSeller': function(event, template) {
        event.preventDefault();
        UpdateSeller($('#inputName').val(),$('#phone').val(),$('#email').val());
    },
    'click #resend-password': function () {
      event.preventDefault();
      if (validateEmail($('#email').val()) === true){

        var findUser = Meteor.users.findOne({_id:$("#id_seller").val()}); 

        Meteor.call('sendEmailSeller',Meteor.user().emails.address,$('#email').val(),findUser.profile.passProvisional, function(err, res){
          if (err){
            Bert.alert( "No se ha podido enviar Email al Remitente!", 'danger' );
          }else{
            Bert.alert( 'Buen Trabajo! Se ha enviado un Email al Nuevo Vendedor!', 'success' );     
          };
        });
      }else{
        Bert.alert( "Porfavor Ingresa un Correo eléctronico válido!", 'danger' );
      }
      
    },

  });

  Template.updateSeller.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('sellers');

  });

  Template.updateSeller.rendered = function(){
    if ($('#status-seller').val()=="activo"){
      $(".switcher-state-on").css("margin-left" , "10%");
      $(".switcher-toggler").css("margin-left" , "39px");
    };
  }

  function UpdateSeller(name,phone,email){
    $(".alert").remove();
    if (isNotEmpty(name) && validateNumber(phone) && validateEmail(email) === true){

      var currentUserId = $("#id_seller").val();
      data = {
        _id: currentUserId,
        name:name,
        phone:phone,
        status:$('#status-seller').val(),
        email:email
      }
     
      Meteor.call('updateUserSeller',data, function(err, res){
        if(res == 1){
          idSeller = res;

          Bert.alert( 'Se ha modificado la información del vendedor!', 'success' );
          Router.go('dashboard');

        }else if( res == 2 ){
          Bert.alert( "Intenta de Nuevo!", 'danger' );
        }
      });

    }else{
      Bert.alert( 'Rellena todos los Campos Correctamente!', 'success' );
    };
  }

}



