if (Meteor.isClient) {
  // counter starts at 0
  Template.registerSeller.events({
    'click #cancelar': function () {
      event.preventDefault();
      Router.go('dashboard');    
    },
    'submit #reisterSeller': function(event, template) {
        event.preventDefault();
        registerSeller($('#inputName').val(),$('#cc').val(),$('#phone').val(),$('#email').val());
    },
  });

  Template.registerSeller.rendered = function(){
    
    
  }

}
function registerSeller(name,cc,phone,email){
  if (isNotEmpty(name) && validateNumber(cc) && validateNumber(phone) && validateEmail(email) === true){

    var currentUserId = Meteor.userId();

    data = {
      createdBy: currentUserId,
      name:name,
      cc:cc,
      phone:phone,
      status:"activo",
      pass:generatePass(8),
      email:email,
      enterprise:Meteor.userId()
    }
    //console.log(JSON.stringify(data));
    Meteor.call('createUserSeller',data, function(err, res){
      if(typeof(res) == 'string'){
        idSeller = res;
        Meteor.call('setRoleToSellerUser',idSeller, function(err, res){
          if (err){
            Bert.alert( "Intenta de Nuevo!", 'danger' );
          }else{

            sendEmailSeller(data.email,data.pass)
            Bert.alert( 'Buen Trabajo! Se ha creado un nuevo vendedor!', 'success' ); 
            Router.go('dashboardSellerAd');
          };
        });
      }else if( res == 2 ){
        Bert.alert( "Este Usuario ya esta registrado!", 'danger' );
      }
    });

  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}

function sendEmailSeller(toEmail,pass){ 
  Meteor.call('sendEmailSeller',Meteor.user().emails.address,toEmail,pass, function(err, res){
    if (err){
      Bert.alert( "No se ha podido enviar Email al Remitente!", 'danger' );
    }else{
      Bert.alert( 'Buen Trabajo! Se ha enviado un Email al Nuevo Vendedor!', 'success' );     
    };
  });

}



