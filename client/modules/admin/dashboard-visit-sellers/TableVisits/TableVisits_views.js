if (Meteor.isClient) {
  // counter starts at 0
  	Template.TableVisits.helpers({
	    visits: function(object) {
	    	return VisitsSeller.find({enterprise:Meteor.userId()});
	    },
        visitsNumber: function(object) {
            return VisitsSeller.find({enterprise:Meteor.userId()}).count();
        },
	    tableSettings : function () {
             return {
                        rowsPerPage: 10,
                        fields: [
                            { 
                                key: 'createdAt', 
                                label: 'Fecha-Visita' ,

                            },
                            { 
                                key: 'createdBy',
                                label: 'Vendedor' 

                            },
                            { 
                                key: 'nameClient',
                                label: 'Cliente', 

                            },
                            { 
                                key: "address", 
                                label: 'Direccion' 
                            },
                            {
                                key:'modifyAt' ,
                                label:'Modificado'
                            },
                            { 
                                key: 'canceled', 
                                label: 'Estado',
                                fn: function (canceled) {
                                    if (canceled == 1) {
                                        var html =  '<button class="btn btn-danger">Cancelada</button>';
                                    }else{
                                        var html =  '<button class="btn btn-success">Activa</button>';
                                    }
                                    return new Spacebars.SafeString(html);
                                }
                            },
                            { 
                                key: '_id', 
                                label: 'Opciones',
                                fn: function (_id, object) {
                                    var html =  '<button id="'+_id+'" class="details-visit btn btn-primary btn-flat">Detalles</button>';
                                    return new Spacebars.SafeString(html);
                                }

                            },
                           
                        ],
       
            };
    	}
	});
}


