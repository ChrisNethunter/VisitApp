if (Meteor.isClient) {
  // counter starts at 0
  Template.detailVisitSellerAdmin.events({
    'click #cancelar': function () {
      event.preventDefault();
      Router.go('dashboardAdminShowVisitsSeller');    
    },
    'click #viewPosition': function () {
      event.preventDefault();
      Router.go('viewPosition',{_IdVisit:Session.get('idVisit')});    
      
    },

 
  });

  Template.detailVisitSellerAdmin.onCreated(function () {
    delete Session.keys['idVisit'];
    Meteor.subscribe('clients-detail-visit');

  });

  Template.detailVisitSellerAdmin.rendered = function(){
      Session.set('idVisit',$('#id').val())
      setTimeout(function() {
        var dataClient = Clients.findOne({_id:$("#name-client").attr("name")});
        $('#phone-client').val(dataClient.phone);
        $('#address-client').val(dataClient.address);    
      }, 1000);
      

  }  

}





