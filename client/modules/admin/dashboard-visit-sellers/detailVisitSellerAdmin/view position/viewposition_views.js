if (Meteor.isClient) {

  var selectedMarkerId = new Blaze.ReactiveVar(null);
  Deps.autorun(function () {
    selectedMarkerId.set(Session.get("currentPhoto"));
  });
  
  Template.viewPosition.helpers({
  	loc: function () {
      // return 0, 0 if the location isn't ready
      return Geolocation.latLng() || { lat: 0, lng: 0 };
    },
    error:function(){
    	return Geolocation.error
    },
    markers: function (){
       return  realTimeLoc.find({});   	
    },
    selectedMarkerId: function(){
    	return selectedMarkerId;
    },
    nameClient: function(id){
      Meteor.subscribe("client",id)
      var find = Clients.findOne({_id:id});

      if (find) {
        return find.name;
      }
      
    }
    
  });
}


