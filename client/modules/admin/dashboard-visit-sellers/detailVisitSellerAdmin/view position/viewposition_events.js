var map, marker, myLatlng;
Template.viewPosition.events({
	
  	'click #vs-reload': function(event){
        event.preventDefault();
        location.reload();
    },
});



Template.viewPosition.rendered = function(){
	setTimeout(function(){ 
		initialize();
	}, 500);
		
	setTimeout(function(){ 
		reloadPositionVisitSeller();
	}, 2000);
}

function reloadPositionVisitSeller(){
	var url =  location.protocol + '//' + location.host;
	var contentString = '<div id="content">'+
				            '<div id="siteNotice">'+
				            '</div>'+
				            '<h5 id="firstHeading" class="firstHeading">'+ $("#infoContent").val() +'</h5>'+
				            '<div id="bodyContent"> <strong>Cliente : </strong>'+
								$("#nameClient").val()		
				            '</div>'+
				         '</div>';

	var infowindow = new google.maps.InfoWindow({
	    content: contentString
	});


   	if (!myLatlng || !myLatlng.lng) {
      myLatlng = new google.maps.LatLng($("#lat").val(), $("#lng").val());
    } 

	var icon = {
	    url: url + "/img/marker.png", // url
	    scaledSize: new google.maps.Size(30, 30), // scaled size
	    origin: new google.maps.Point(0,0), // origin
	    anchor: new google.maps.Point(0, 0) // anchor
	};

    if (!marker || !marker.setPosition) {
        marker = new google.maps.Marker({
            position: myLatlng,
            title: "Vendedor",
            icon:icon ,
        });
        map.setZoom(15);
        // To add the marker to the map, call setMap();
        marker.setMap(map);
    } else {
        marker.setPosition(myLatlng);
    }

    marker.addListener('click', function() {
	    infowindow.open(map, marker);
	});
}

function initialize() {
    var mapOptions = {
        center: {
            lat: 4.814631,
            lng: -75.692930
        },
        zoom: 15
    };

    map = new google.maps.Map(document.getElementById('map'), mapOptions);
}

  