if (Meteor.isClient) {
  Template.dashboardMap.events({
  	'click #refresh-map': function(event){
      event.preventDefault();
      onSuccess(user,$("#lat").val(),$("#lng").val());
    },
    'click #select-seller': function(event){
      event.preventDefault();
      $("#sellers-map").toggle("slow");
    },
    'click #add-asignature': function(event){
        event.preventDefault();
        $("#signature-pad").slideDown();
        signaturePad();  
    }
    
  });

  Template.dashboardMap.rendered = function(){
    
    setTimeout(function(){ 
      
      lat =$("#lat").val();
      lng =$("#lng").val(); 
      onSuccess(user,lat,lng);

    }, 3000);
    
    $('body').removeClass( "page-signup" )
  }

  var onSuccess = function (user,lat,lng) {
    
    if (Meteor.user().roles[0] !== "admin"){
      var data =  {
                  user:Meteor.user()._id,
                  username:Meteor.user().profile.fullname,
                  createAt:new Date(),
                  marker: {
                    lat: lat,
                    lng: lng,
                    infoWindowContent: "<div><h3>"+Meteor.user().profile.fullname+"</h3><br><p>"+new Date()+"</p></div>"
                  }
                  
                }

      Meteor.call('saveLocation',data, function(err, res){   
        if(res == 1){
         console.log("bien")
        }else{
          console.log("mal")
        }
      });
    }else{
      
      console.log("admin")
    };
    /**/
  };
}


