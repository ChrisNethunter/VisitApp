Template.inventory.events({
	'click #register': function(event){
      event.preventDefault();
      Router.go('register-inventory');
  },
  'click .update': function(event){
      event.preventDefault();
      Router.go('update-inventory',{_IdInventory:event.currentTarget.id});
  },
});

Template.inventory.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('inventory',Meteor.userId());

});

Template.inventory.rendered = function(){

}

