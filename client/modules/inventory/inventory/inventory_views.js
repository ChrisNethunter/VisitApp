Template.inventory.helpers({
    showInventory: function(object) {
       return Inventory.find({user:Meteor.userId()});
    },

    number: function(object) {
       return Inventory.find({user:Meteor.userId()}).count();
    },

    tableSettings : function () {
             return {
                        rowsPerPage: 10,
                        fields: [

                            { 
                                key: 'product', 
                                label: 'Producto',
                                fn: function (product, object) {
                                    Meteor.subscribe("product",product);
                                    var find = Products.findOne({_id:product});
                                    if (find){
                                        return new Spacebars.SafeString(find.name);
                                    }
                                }
                            },
                            { 
                                key: 'product', 
                                label: 'Proveedor',
                                fn: function (product, object) {
                                    Meteor.subscribe("product",product);
                                    var find = Products.findOne({_id:product});
                                    if (find){
                                        Meteor.subscribe("provider",find.provider);
                                        var findDataProvider = Providers.findOne({_id:find.provider});
                                        if (findDataProvider) {
                                            return new Spacebars.SafeString(findDataProvider.name);
                                        }
                                        
                                    } 
                                }
                            },
                            { 
                                key: 'quantity', 
                                label: 'Cantidad Inicial' ,
                            },
                            { 
                                key: 'actualQuantity', 
                                label: 'Cantidad Actual' ,
                            },
                            { 
                                key: 'description', 
                                label: 'Descripción' ,

                            },
                            { 
                                key: "createdAt", 
                                label: 'Creado' 

                            },
                            { 
                                key: "status", 
                                label: 'Estado',
                                fn: function (estado, object) {
                                    if (estado == "activo"){
                                        var html =  '<div id="activo" class="btn btn-success btn-sm">Activo</div>';
                                        
                                    }else{
                                        var html =  '<div id="inactivo" class="btn btn-danger btn-sm">Inactivo</div>';
                                    };
                                    return new Spacebars.SafeString(html);
                                }
                            },
                            { 
                                key: '_id', 
                                label: 'Opciones',
                                fn: function (_id, object) {
                                    var html =  '<button id="'+_id+'" class="btn btn-primary btn-flat update">Modificar</button>';
                                    return new Spacebars.SafeString(html);
                                }

                            }, 
                        ],
       
            };
    }

});
