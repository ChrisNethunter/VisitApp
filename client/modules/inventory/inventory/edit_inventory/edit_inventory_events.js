var Temp
if (Meteor.isClient) {
  
  Template.edit_inventory.events({
    'click #cancelar': function () {
      event.preventDefault();
      Router.go('products');    
    },
    'click .switcher-inner': function(event, template) {
      if ($('#status-seller').val()=="activo"){
        $('#status-seller').val("inactivo");
        $(".switcher-state-on").css("margin-left" , "-100%");
        $(".switcher-toggler").css("margin-left" , "1px");
      }else{
        $('#status-seller').val("activo");
        $(".switcher-state-on").css("margin-left" , "10%");
        $(".switcher-toggler").css("margin-left" , "39px");
      };
    },
    'submit #reister': function(event, template) {
        event.preventDefault();
        edit( 
              $('#product').val() , 
              $('#price-buy').val() , 
              $('#quantity').val() , 
              $('#description').val()
            );
    },
  });

  Template.edit_inventory.rendered = function(){

    setTimeout(function(){ 
      if ($('#status-seller').val()=="activo"){
          $(".switcher-state-on").css("margin-left" , "10%");
          $(".switcher-toggler").css("margin-left" , "39px");
      };

      $("#product").val($("#id-pro").val());

      var findProduct = Products.findOne({_id:$("#id-pro").val()});
      var provider = Providers.findOne({_id:findProduct.provider}); 
      $("#provider").val(provider.name);
      $("#price").val(findProduct.price);


      Meteor.call('sumInventory', $("#id-pro").val(), function(error, quantity) {
          if (!error) {  
            Temp = Number(quantity) - Number($("#quantity").val());
          }
      });
    }, 500);
  }
}

function edit(product,priceBuy,quantity,description){

  if (isNotEmpty(product) && validateNumber(quantity) && isNotEmpty(description) && validateNumber(priceBuy) === true){

    var calc = Number(Temp) + Number(quantity);

    data =  {
              _id:document.getElementById("ic-ct").value,
              product:product,
              priceBuy:Number(priceBuy),
              quantity:Number(quantity),
              actualQuantity:Number(quantity),
              description:description,

            };

    Meteor.call('inventoryEdit',data, function(err, res){
      if (err){
        Bert.alert( "Intenta de Nuevo! " + error, 'danger' );
      }else{
        updateCalcInventory(product,calc)
        //Router.go('products');
      };
    });

  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}

function updateCalcInventory (product,newValueCalc) {

    Meteor.call('updateInventoryCalc', product,newValueCalc, function(error, result) {
        if (error) {  
          Bert.alert( "Intenta de Nuevo! " + error, 'danger' );
        }else{
          Bert.alert( 'Se insertaron los datos Correctamente', 'success' ); 
          Router.go('inventory');    
        }
    });

}



