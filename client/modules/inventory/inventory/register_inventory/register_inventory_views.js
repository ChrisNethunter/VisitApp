Template.register_inventory.helpers({
    showCategories: function() {
       return Categories.find();
    },
    showProducts: function() {
       return Products.find({categorie:Session.get("categorie")});
    },

});

Template.register_inventory.onCreated(function () {
  // Use this.subscribe inside onCreated callback

  Meteor.subscribe('allCategories',Meteor.userId());
  //Meteor.subscribe('providersAlls',Meteor.userId());
});
