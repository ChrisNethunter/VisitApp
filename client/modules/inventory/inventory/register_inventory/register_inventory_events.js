if (Meteor.isClient) {
  // counter starts at 0
  Template.register_inventory.events({
    'click #cancelar': function(event){
      event.preventDefault();
      Router.go('inventory');
    },
    'submit #register': function(event, template) {
        event.preventDefault();
          
          validateExistProductInventory( $('#product').val() );

    },

    'change #categorie': function(event){
      event.preventDefault();
      Session.set("categorie",$("#categorie").val())
      Meteor.subscribe("productCategorie",$("#categorie").val());

      setTimeout(function(){ 
        $("#register").slideDown();
      }, 500);
    },

    'change #product': function(event){
      event.preventDefault();
      var findProduct = Products.findOne({_id:$("#product").val()});
      if (findProduct){
        Meteor.subscribe("provider",findProduct.provider);
        setTimeout(function(){ 
          var findDataProvider = Providers.findOne({_id:findProduct.provider});
          $("#provider").val(findDataProvider.name);
          $("#price").val(findProduct.price);
        }, 500); 
      }
    },
  });

  Template.register_inventory.rendered = function(){

  }

}
function validateExistProductInventory(product){
    Meteor.call('validateExistInventory', product , function(error, result) {
        if (error) {  
            Bert.alert( "Intenta de Nuevo! " + err, 'danger' );
        }else{
            if (result > 0){
                insertData( 
                            $('#product').val() , 
                            $('#price-buy').val() , 
                            $('#quantity').val() , 
                            $('#description').val(),
                            1
                          );
                //insertData(data,1)
            }else{
                insertData( 
                            $('#product').val() , 
                            $('#price-buy').val() , 
                            $('#quantity').val() , 
                            $('#description').val(),
                            0
                          );
                //insertData(data,0)
            };
        }
    });
}


function insertData(product,priceBuy,quantity,description,status){

  var date = new Date();
  if (isNotEmpty(product) && validateNumber(quantity) && isNotEmpty(description) && validateNumber(priceBuy) === true){

    var createAt = dateString();
    var data =  {
                  user: Meteor.userId(), 
                  product:product,
                  priceBuy:Number(priceBuy),
                  quantity:Number(quantity),
                  actualQuantity:Number(quantity),
                  description:description,
                  status:"activo",
                  createdAt: createAt["stringDate"],
                  date:date,
                }

    //console.log(JSON.stringify(data));
    Meteor.call('inventoryInsert',data, function(err, res){
      if (err){
        Bert.alert( "Intenta de Nuevo! " + err, 'danger' );
      }else{

        var data =  {
                      product:product,
                      userInit:Meteor.userId(),
                      quantity:Number(quantity),
                      createAt: new Date(),
                    };

        initCalcInventory(data,status)
        /*Meteor.call('initCalcInventory',res,quantity, function(err, res){
            if (!err){
              sweetAlert("Buen Trabajo!", "Se ha registrado al Inventario Correctamente!", "success");
              Router.go('inventory');
            }
        });*/
      };
    });

  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}

function initCalcInventory(data,status){

    if (status == 1){
        data.modifyAt = new Date();

        Meteor.call('updateCalcInventory', data , function(error, result) {
            if (error) {  
                Bert.alert( "Intenta de Nuevo! " + error, 'danger' );

            }else{
                Bert.alert( 'Se insertaron los datos Correctamente', 'success' ); 
                Router.go('inventory'); 
            }
        });   
    }else{
        Meteor.call('initCalcInventory', data , function(error, result) {
            if (error) {  
                Bert.alert( "Intenta de Nuevo! " + error, 'danger' );
            }else{
                Bert.alert( 'Se insertaron los datos Correctamente', 'success' ); 
                Router.go('inventory'); 
               
            }
        });

    };  
}




