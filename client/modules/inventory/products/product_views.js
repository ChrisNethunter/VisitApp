Template.products.helpers({
    showProducts: function(object) {
       return Products.find({user:Meteor.userId()});
    },

    number: function(object) {
       return Products.find({user:Meteor.userId()}).count();
    },

    tableSettings : function () {
             return {
                        rowsPerPage: 10,
                        fields: [
                            { 
                                key: 'name', 
                                label: 'Nombre' ,

                            },
                            { 
                                key: 'categorie', 
                                label: 'Categoria',
                                fn: function (categorie, object) {
                                    Meteor.subscribe("categorie",categorie);
                                    var find = Categories.findOne({_id:categorie})
                                    if (find){
                                        var html = find.name
                                        return new Spacebars.SafeString(html);
                                    }
                                    
                                }
                            },
                            { 
                                key: 'provider', 
                                label: 'Proveedor',
                                fn: function (provider, object) {
                                    Meteor.subscribe("provider",provider);
                                    var find = Providers.findOne({_id:provider})
                                    if (find){
                                        var html = find.name
                                        return new Spacebars.SafeString(html);
                                    }
                                    
                                }
                            },
                            { 
                                key: 'price', 
                                label: 'Precio de Venta' ,

                            },
                            { 
                                key: 'description', 
                                label: 'Observaciones' ,

                            },
                            { 
                                key: "createdAt", 
                                label: 'Creado' 

                            },
                            { 
                                key: "status", 
                                label: 'Estado',
                                fn: function (estado, object) {
                                    if (estado == "activo"){
                                        var html =  '<div id="activo" class="btn btn-success btn-sm">Activo</div>';
                                        
                                    }else{
                                        var html =  '<div id="inactivo" class="btn btn-danger btn-sm">Inactivo</div>';
                                    };
                                    return new Spacebars.SafeString(html);
                                }
                            },
                            { 
                                key: '_id', 
                                label: 'Opciones',
                                fn: function (_id, object) {
                                    var html =  '<button id="'+_id+'" class="btn btn-primary btn-flat update">Modificar</button>';
                                    return new Spacebars.SafeString(html);
                                }

                            }, 
                        ],
       
            };
    }

});
