Template.edit_product.helpers({
    showCategories: function() {
       return Categories.find({user:Meteor.userId()});
    },
    showProviders: function() {
       return Providers.find({user:Meteor.userId()});
    },
});

Template.edit_product.onCreated(function () {
  // Use this.subscribe inside onCreated callback
  Meteor.subscribe('categories',Meteor.userId());
  Meteor.subscribe('providers',Meteor.userId());
  

});



