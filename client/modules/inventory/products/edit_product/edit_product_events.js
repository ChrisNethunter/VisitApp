if (Meteor.isClient) {

  Template.edit_product.events({
    'click #cancelar': function () {
      event.preventDefault();
      Router.go('products');    
    },
    'click .switcher-inner': function(event, template) {
      if ($('#status-seller').val()=="activo"){
        $('#status-seller').val("inactivo");
        $(".switcher-state-on").css("margin-left" , "-100%");
        $(".switcher-toggler").css("margin-left" , "1px");
      }else{
        $('#status-seller').val("activo");
        $(".switcher-state-on").css("margin-left" , "10%");
        $(".switcher-toggler").css("margin-left" , "39px");
      };
    },
    'submit #reister': function(event, template) {
        event.preventDefault();
        edit($('#name').val() , $('#categorie').val() , $('#price').val() , $('#description').val(), $("#provider").val());
    },
  });

  Template.edit_product.rendered = function(){

    setTimeout(function(){ 
      if ($('#status-seller').val()=="activo"){
          $(".switcher-state-on").css("margin-left" , "10%");
          $(".switcher-toggler").css("margin-left" , "39px");
      };

      $("#categorie").val($("#id-cate").val());
      $("#provider").val($("#id-pro").val());

    }, 500);
  }
}

function edit(name,categorie,price,description,provider){

  if (isNotEmpty(name) && isNotEmpty(categorie) && isNotEmpty(description) && validateNumber(price) === true){

    var createAt = dateString();

    data =  {
              _id:document.getElementById("ic-ct").value,
              name:name,
              categorie:categorie,
              provider:provider,
              price:Number(price),
              description:description,
              status:$('#status-seller').val(),
            };

    //console.log(JSON.stringify(data))
    Meteor.call('productEdit',data, function(err, res){
      if (err){
        Bert.alert( "Intenta de Nuevo!", 'danger' );
      }else{;
        Bert.alert( 'Se ha editado correctamente el Producto!', 'success' ); 
        Router.go('products');
      };
    });

  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}




