Template.products.events({
	'click #register': function(event){
      event.preventDefault();
      Router.go('register-product');
  },
  'click .update': function(event){
      event.preventDefault();
      Router.go('update-product',{_IdProduct:event.currentTarget.id});
  },
});

Template.products.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('products',Meteor.userId());

});

Template.products.rendered = function(){
  setTimeout(function(){ 
    
  }, 1000); 
	  
}

