Template.register_product.helpers({
    showCategories: function() {
       return Categories.find();
    },
    showProviders: function() {
       return Providers.find();
    },
});

Template.register_product.onCreated(function () {
  // Use this.subscribe inside onCreated callback
  Meteor.subscribe('allCategories',Meteor.userId());
  Meteor.subscribe('providersAlls',Meteor.userId());
});
