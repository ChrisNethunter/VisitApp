if (Meteor.isClient) {
  // counter starts at 0
  Template.register_product.events({
    'click #cancelar': function(event){
      event.preventDefault();
      Router.go('products');
    },
    'submit #reister': function(event, template) {
        event.preventDefault();
        insertData($('#name').val() , $('#categorie').val() , $('#price').val() , $('#description').val() , $("#provider").val());
    },
  });

  Template.register_product.rendered = function(){

  }

}

function insertData(name,categorie,price,description,provider){

  var date = new Date();
  if (isNotEmpty(name) && isNotEmpty(categorie) && isNotEmpty(description) && validateNumber(price) === true){

    var createAt = dateString();
    data = {
      user: Meteor.userId(), 
      name:name,
      categorie:categorie,
      provider:provider,
      price:Number(price),
      description:description,
      status:"activo",
      createdAt: createAt["stringDate"],
      date:date,
    }

    //console.log(JSON.stringify(data));
    Meteor.call('productInsert',data, function(err, res){
      if (err){
        Bert.alert( "Intenta de Nuevo!", 'danger' );
      }else{
        Bert.alert( 'Se ha registrado una nuevo Producto!', 'success' );
        Router.go('products');
      };
    });

  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
    
  };
}




