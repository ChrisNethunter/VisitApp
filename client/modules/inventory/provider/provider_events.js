Template.provider.events({
	'click #register': function(event){
      event.preventDefault();
      Router.go('register-provider');
  },
  'click .update': function(event){
      event.preventDefault();
      Router.go('update-provider',{_IdProvider:event.currentTarget.id});
  },
});

Template.provider.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('providers',Meteor.userId());

});

Template.provider.rendered = function(){
  setTimeout(function(){ 
    
  }, 1000); 
	  
}

