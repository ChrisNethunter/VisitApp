if (Meteor.isClient) {

  Template.edit_provider.events({
    'click #cancelar': function () {
      event.preventDefault();
      Router.go('providers');    
    },
    'click .switcher-inner': function(event, template) {
      if ($('#status-seller').val()=="activo"){
        $('#status-seller').val("inactivo");
        $(".switcher-state-on").css("margin-left" , "-100%");
        $(".switcher-toggler").css("margin-left" , "1px");
      }else{
        $('#status-seller').val("activo");
        $(".switcher-state-on").css("margin-left" , "10%");
        $(".switcher-toggler").css("margin-left" , "39px");
      };
    },
    'submit #reisterCategorie': function(event, template) {
        event.preventDefault();
        edit($('#name').val() , $('#phone').val() , $('#description').val());
    },
  });

  Template.edit_provider.rendered = function(){

    setTimeout(function(){ 
      if ($('#status-seller').val()=="activo"){
          $(".switcher-state-on").css("margin-left" , "10%");
          $(".switcher-toggler").css("margin-left" , "39px");
      };
    }, 500);
  }
}

function edit(name,phone,description){

  if (isNotEmpty(name) === true){

    var createAt = dateString();

    data =  {
              _id:document.getElementById("ic-ct").value,
              name:name,
              phone:phone,
              description:description,
              status:$('#status-seller').val(),
            };

    //console.log(JSON.stringify(data))
    Meteor.call('providerEdit',data, function(err, res){
      if (err){
        Bert.alert( "Intenta de Nuevo!", 'danger' );
      }else{
        Bert.alert( 'Se ha editado correctamente el proveedor!', 'success' ); 
        Router.go('providers');
      };
    });

  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}




