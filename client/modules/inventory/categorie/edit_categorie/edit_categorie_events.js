if (Meteor.isClient) {

  Template.edit_categorie.events({
    'click #cancelar': function () {
      event.preventDefault();
      Router.go('categories');    
    },
    'click .switcher-inner': function(event, template) {
      if ($('#status-seller').val()=="activo"){
        $('#status-seller').val("inactivo");
        $(".switcher-state-on").css("margin-left" , "-100%");
        $(".switcher-toggler").css("margin-left" , "1px");
      }else{
        $('#status-seller').val("activo");
        $(".switcher-state-on").css("margin-left" , "10%");
        $(".switcher-toggler").css("margin-left" , "39px");
      };
    },
    'submit #reisterCategorie': function(event, template) {
        event.preventDefault();
        edit($('#name').val());
    },
  });

  Template.edit_categorie.rendered = function(){

    setTimeout(function(){ 
      if ($('#status-seller').val()=="activo"){
          $(".switcher-state-on").css("margin-left" , "10%");
          $(".switcher-toggler").css("margin-left" , "39px");
      };
    }, 500);
  }
}

function edit(name){

  if (isNotEmpty(name) === true){

    var createAt = dateString();

    data =  {
              _id:document.getElementById("ic-ct").value,
              name:name,
              status:$('#status-seller').val(),
            };

    //console.log(JSON.stringify(data))
    Meteor.call('categorieEdit',data, function(err, res){
      if (err){
        Bert.alert( "Intenta de Nuevo!", 'danger' );
      }else{
        Bert.alert( 'Se ha editado Correctamente los datos!', 'success' ); 
        Router.go('categories');
      };
    });
  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}




