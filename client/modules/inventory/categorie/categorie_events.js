Template.categorie.events({
	'click #register': function(event){
      event.preventDefault();
      Router.go('register-categorie');
  },
  'click .update': function(event){
      event.preventDefault();
      Router.go('update-categorie',{_IdCategorie:event.currentTarget.id});
  },
});

Template.categorie.onCreated(function () {
  // Use this.subscribe inside onCreated callback
  Meteor.subscribe('categories',Meteor.userId());

});

Template.categorie.rendered = function(){
}

