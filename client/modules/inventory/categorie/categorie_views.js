Template.categorie.helpers({
    showCategories: function(object) {
       return Categories.find({user:Meteor.userId()});
    },
    numberCategories: function(object) {
       return Categories.find({user:Meteor.userId()}).count();
    },

    tableSettings : function () {
             return {
                        rowsPerPage: 10,
                        fields: [
                            { 
                                key: 'name', 
                                label: 'Categoria' ,

                            },
                            { 
                                key: "createdAt", 
                                label: 'Creado' 
                            },
                            { 
                                key: "status", 
                                label: 'Estado',
                                fn: function (estado, object) {
                                    if (estado == "activo"){
                                        var html =  '<div id="activo" class="btn btn-success btn-sm">Activo</div>';
                                        
                                    }else{
                                        var html =  '<div id="inactivo" class="btn btn-danger btn-sm">Inactivo</div>';
                                    };
                                    return new Spacebars.SafeString(html);   
                                }
                            },
                            { 
                                key: '_id', 
                                label: 'Opciones',
                                fn: function (_id, object) {
                                    var html =  '<button id="'+_id+'" class="btn btn-primary btn-flat update">Modificar</button>';
                                    return new Spacebars.SafeString(html);
                                }

                            }, 
                        ],
       
            };
    }

});
