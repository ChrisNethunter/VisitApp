if (Meteor.isClient) {
  // counter starts at 0
  Template.register_categorie.events({
    'click #cancelar': function(event){
      event.preventDefault();
      Router.go('categories');
    },
    'submit #reisterCategorie': function(event, template) {
        event.preventDefault();
        insertData($('#name').val());
    },
  });

  Template.register_categorie.rendered = function(){
  }

}

function insertData(name){
  var date = new Date();
  if (isNotEmpty(name) === true){

    var createAt = dateString();
    data = {
      user: Meteor.userId(), 
      name:name,
      status:"activo",
      createdAt: createAt["stringDate"],
      date:date,

    }
    //console.log(JSON.stringify(data));
    Meteor.call('categorieInsert',data, function(err, res){
      if (err){
        Bert.alert( "Intenta de Nuevo!", 'danger' );
      }else{
        Bert.alert( 'Se ha registrado una nueva Categoria!', 'success' ); 
        Router.go('categories');
      };
    });

  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}




