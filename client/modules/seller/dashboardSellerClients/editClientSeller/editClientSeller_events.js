if (Meteor.isClient) {

  Template.editClientSeller.events({
    'click #cancelar': function () {
      event.preventDefault();
      Router.go('dashboardSellerClients');    
    },
    'click .switcher-inner': function(event, template) {
      if ($('#status-seller').val()=="activo"){
        $('#status-seller').val("inactivo");
        $(".switcher-state-on").css("margin-left" , "-100%");
        $(".switcher-toggler").css("margin-left" , "1px");
      }else{
        $('#status-seller').val("activo");
        $(".switcher-state-on").css("margin-left" , "10%");
        $(".switcher-toggler").css("margin-left" , "39px");
      };
    },
    'submit #reisterClient': function(event, template) {
        event.preventDefault();
        editClientSeller($('#name').val(),$('#nit').val(),$('#phone').val(),$('#address').val());
    },
  });

  Template.editClientSeller.rendered = function(){

    setTimeout(function(){ 
      if ($('#status-seller').val()=="activo"){
        $(".switcher-state-on").css("margin-left" , "10%");
        $(".switcher-toggler").css("margin-left" , "39px");
      };

    }, 500);
    
  }

}

function editClientSeller(name,nit,phone,address){
  $(".alert").remove();
  if (isNotEmpty(name) && validateNumber(nit) && validateNumber(phone) && isNotEmpty(address)  === true){

    var createAt = dateString();

    data =  {
              _id:document.getElementById("ic-cl").value,
              nit:nit,
              name:name,
              phone:phone,
              address:address,
              status:$('#status-seller').val(),
            };

    //console.log(JSON.stringify(data))
    Meteor.call('updateDataClient',data, function(err, res){
      if (err){
        Bert.alert( "Intenta de Nuevo!", 'danger' );
      }else{
        Bert.alert( 'Se ha editado correctamente el cliente!', 'success' ); 
        Router.go('dashboardSellerClients');
      };
    });

  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}




