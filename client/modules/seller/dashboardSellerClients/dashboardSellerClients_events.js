Template.dashboardSellerClients.rendered = function(){

}

Template.dashboardSellerClients.events({
	'click #register-client': function(event){
      event.preventDefault();
      Router.go('register-client');
  },

  'click .update-client': function(event){
      event.preventDefault();

      Router.go('edit-client',{_IdClient:event.currentTarget.id});
  },
});

Template.dashboardSellerClients.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('userInfo',Meteor.userId());

});

  Template.dashboardSellerClients.rendered = function(){
    setTimeout(function(){ 
      if (Meteor.userId()){
        Meteor.subscribe('clients-detail-visit',Meteor.userId());
        /*var userInfo = Meteor.users.findOne({_id:Meteor.userId()});
      
        if (userInfo.roles == "admin"){
          Meteor.subscribe('clients-detail-visit',Meteor.userId());
        }else{
          Meteor.subscribe('clients',Meteor.userId());
        }; */
      }
    }, 1000); 
 	  
  }

