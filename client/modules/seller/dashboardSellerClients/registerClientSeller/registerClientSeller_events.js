if (Meteor.isClient) {
  // counter starts at 0
  Template.registerClientSeller.events({
    'click #cancelar': function () {
      event.preventDefault();
      Router.go('dashboard');    
    },
    'submit #reisterClient': function(event, template) {
        event.preventDefault();
        registerClientSeller($('#name').val(),$('#nit').val(),$('#phone').val(),$('#address').val());
    },
  });

  Template.registerClientSeller.rendered = function(){
  }

}

function registerClientSeller(name,nit,phone,address){
  $(".alert").remove();
  var date = new Date();
  if (isNotEmpty(name) && validateNumber(nit) && validateNumber(phone) && isNotEmpty(address)  === true){


    var createAt = dateString();

    data = {
      enterprise: Meteor.userId(),
      nit:nit,
      name:name,
      phone:phone,
      status:"activo",
      address:address,
      createdAt: createAt["stringDate"],
      date:date,

    }
    //console.log(JSON.stringify(data));
    Meteor.call('saveDataClient',data, function(err, res){
      if (err){
        Bert.alert( "Intenta de Nuevo!", 'danger' );
      }else{
        Bert.alert( "Se ha registrado un nuevo cliente!", 'success' );
        Router.go('dashboardSellerClients');
      };
    });
  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}




