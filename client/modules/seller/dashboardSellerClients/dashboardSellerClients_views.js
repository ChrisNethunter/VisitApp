Template.dashboardSellerClients.helpers({
    showClients: function(object) {
       return Clients.find();
    },
    clientsNumber: function(object) {
       return Clients.find().count();
    },

    tableSettings : function () {
             return {
                        rowsPerPage: 10,
                        fields: [
                            { 
                                key: 'nit', 
                                label: 'Nit' ,

                            },
                            { 
                                key: 'name',
                                label: 'Nombre' 

                            },
                            { 
                                key: "address", 
                                label: 'Direccion' 
                            },
                            { 
                                key: "phone", 
                                label: 'Teléfono' 

                            },
                            { 
                                key: "createdAt", 
                                label: 'createdAt' 

                            },
                            { 
                                key: "status", 
                                label: 'Estado',
                                fn: function (estado, object) {
                                    if (estado == "activo"){
                                        var html =  '<div id="activo" class="btn btn-success btn-sm">Activo</div>';
                                        
                                    }else{
                                        var html =  '<div id="inactivo" class="btn btn-danger btn-sm">Inactivo</div>';
                                    };
                                    return new Spacebars.SafeString(html);
                                    
                                }

                            },
                            { 
                                key: '_id', 
                                label: 'Opciones',
                                fn: function (_id, object) {
                                    var html =  '<button id="'+_id+'" class="btn btn-primary btn-flat update-client">Modificar</button>';
                                    return new Spacebars.SafeString(html);
                                }

                            }, 
                        ],
       
            };
    }

});
