if (Meteor.isClient) {
  user = Meteor.userId();

  Template.sellerDashboard.events({
    'click #change-pass-seller' : function(event, t){
      event.preventDefault();
      changePass($('#current-pass').val(),$('#pass').val());  
    },
  	
  });

  Template.sellerDashboard.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    //Router.go('dashboardSellerClients');
    Meteor.subscribe('userInfo',Meteor.userId());  
  });

  Template.sellerDashboard.rendered = function(){
    var userInfo = Meteor.users.findOne({_id:Meteor.userId()});
    if (userInfo.profile.request == "change-pass"){
      $('.pass-seller').click();
    }else{
      Router.go('dashboardSellerClients');
    };
    //$('body').removeClass( "page-signup" )
  }
}

function changePass(current,pass){
  Accounts.changePassword(current,pass, function(error) {
    if (error) {
        Bert.alert( "Intenta de Nuevo! " + error, 'danger' );
    } else {
      Meteor.call('updateRequestUser',Meteor.userId(), function(err, res){
        if (!err){
          Bert.alert( 'Se ha Cambiado la Contraseña!', 'success' ); 
          location.reload();
          //$("#close").click()
          //Router.go("dashboardSellerRutes")
        };
      });
    }
  });

}
