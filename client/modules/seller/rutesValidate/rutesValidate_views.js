Template.rutesValidate.helpers({
  	showRutes: function(object) {
       return Rutes.find({seller:Meteor.userId()});
    },
    rutesNumber: function(object) {
       return Rutes.find({seller:Meteor.userId()}).count();
    },

    tableSettings : function () {
             return {
                        rowsPerPage: 10,
                        fields: [
                            { 
                                key: "createdAt", 
                                label: 'Fecha' 

                            },
                            { 
                                key: "rute", 
                                label: 'Ruta' 

                            },
                            { 
                                key: '_id', 
                                label: 'Estado',
                                fn: function (_id, object) {
                                    var html =  '<button id="'+_id+'" class="btn btn-success">Estado</button>';
                                    return new Spacebars.SafeString(html);
                                }

                            },
                            { 
                                key: '_id', 
                                label: 'Opciones',
                                fn: function (_id, object) {
                                    var html =  '<button id="'+_id+'" class="btn btn-primary btn-flat detail-rute">Detalles</button>';
                                    return new Spacebars.SafeString(html);
                                }

                            },
                            
                           
                        ],
       
            };
    }
});



