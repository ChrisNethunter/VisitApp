Template.detailRute.events({
  'click #cancelar': function (event) {
    event.preventDefault();
    Router.go('programRutes');    
  },
  'submit #ruteForm': function(event, template) {
      event.preventDefault();

      var data =  {
                    rute: $("#rute").val(),
                    seller:$("#seller").val(),
                    clients :[],
                    createdAt: dateString()["stringDate"], 
                    date: new Date(),
                    user:Meteor.userId(),
                  };


      $('#checkClients input:checked').each(function() {

          data.clients.push(  {
                                idClient:$(this).val(),
                                name:$(this).attr('name'),
                              }
                            );
      });

      registerRoute(data);
  },
  'click .register-visit': function (event) {
    event.preventDefault();
    Session.set("dataClient",event.currentTarget.id);
    Router.go('register-visit-par-inventory',{_IdCliente:event.currentTarget.id, _Idroute:$("#ruteId").val()}); 

  },

});

Template.detailRute.onCreated(function () {
  //Meteor.subscribe("sellers");
  //Meteor.subscribe("allClients");
});

Template.detailRute.rendered = function(){
  setTimeout(function(){ 
    Meteor.subscribe("rute",$("#ruteId").val())
    checkEdit()
  }, 500);
}

function checkEdit(){
  var rutes = Rutes.findOne({_id:$("#ruteId").val()});
  $
  for (var i=0; i < Object.keys(rutes.clients).length; i++){
      $("#" + rutes.clients[i].idClient ).prop( "checked", true );
  }
  //console.log("rutes " + JSON.stringify(rutes))
}



