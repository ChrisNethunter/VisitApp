Template.detailRute.helpers({
	showSellers: function() {
       return Meteor.users.find();
    },
    showClients: function() {
       return Clients.find({});
    },
    statusButton:function(status,_id){
    	var html;

    	if (status == "visitado"){
    		var html = ' <button type="button" class="btn btn-success">Visitado</button> '
    	}else{
    		var html = ' <button type="button" class="btn btn-danger">Pendiente</button> '
    	};

    	return new Spacebars.SafeString(html)
    },
    addressClient:function(idClient){
    	Meteor.subscribe("client", idClient)
    	var data = Clients.findOne({_id:idClient});
        if (data){
            return data.address;
        }
    },

    validateVisit:function(status,_id){
        if (status == "visitado"){
            return ""
        }else{
            return _id
        };
    },

    disable:function(status,_id){
        if (status == "visitado"){
            return "disabled"
        }
    }

    
});



