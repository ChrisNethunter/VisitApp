Template.inventoryVisitSeller.helpers({
    showVisits: function(object) {
       return VisitsSeller.find({createdById:Meteor.userId()});
    },
    visitsNumber: function(object) {
       return VisitsSeller.find({createdById:Meteor.userId()}).count();
    },

    tableSettings : function () {
             return {
                        rowsPerPage: 6,
                        fields: [
                            { 
                                key: 'createdAt', 
                                label: 'Fecha-Visita' ,

                            },
                            { 
                                key: 'createdBy',
                                label: 'Vendedor' 

                            },
                            { 
                                key: 'idClient', 
                                label: 'Cliente',
                                fn: function (idClient, object) {
                                    Meteor.subscribe("client",idClient);
                                    var find = Clients.findOne({_id:idClient});
                                    if (find){
                                        return new Spacebars.SafeString(find.name);
                                    }
                                }
                            },
                            { 
                                key: 'idClient', 
                                label: 'Dirección',
                                fn: function (idClient, object) {
                                    Meteor.subscribe("client",idClient);
                                    var find = Clients.findOne({_id:idClient});
                                    if (find){
                                        return new Spacebars.SafeString(find.address);
                                    }
                                } 
                            },
                            {
                                key:'modifyAt' ,
                                label:'Modificado'
                            },
                            { 
                                key: 'canceled', 
                                label: 'Estado',
                                fn: function (canceled) {
                                    if (canceled == 1) {
                                        var html =  '<button class="btn btn-danger">Cancelada</button>';
                                    }else{
                                        var html =  '<button class="btn btn-success">Activa</button>';
                                    }
                                    return new Spacebars.SafeString(html);
                                }
                            },
                            { 
                                key: '_id', 
                                label: 'Opciones',
                                fn: function (_id, object) {
                                    var html =  '<button id="'+_id+'" class="btn btn-primary btn-flat details-visit-inventory">Detalles</button>';
                                    return new Spacebars.SafeString(html);
                                }

                            },
                           
                        ],
       
            };
    }

});
