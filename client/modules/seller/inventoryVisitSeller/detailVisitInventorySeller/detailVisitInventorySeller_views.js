if (Meteor.isClient) {
  Template.detailVisitInventorySeller.helpers({
  	productname: function(id) {
  		Meteor.subscribe("product",id);
        var find = Products.findOne({_id:id});

        if (find){
        	return find.name
        }
    },

    productPrice: function(id) {
  		Meteor.subscribe("product",id);
        var find = Products.findOne({_id:id});

        if (find){
        	return find.price
        }
    },

    productQuantity: function(id) {
  		Meteor.subscribe("inventoryCalcSelectProduct",id);
        var find = calcInventory.findOne({product:id});

        if (find){
        	return find.quantity
        }
    },

    totalProduct: function(id,quantity) {
  		Meteor.subscribe("product",id);
        var find = Products.findOne({_id:id});

        if (find){
        	var calc = Number(find.price)*Number(quantity);
        	return "$ " + calc
        }
    },

    client: function(id) {
  		Meteor.subscribe("client",id);
        var find = Clients.findOne({_id:id});

        if (find){
        	return find.name
        }
    },

    

  });
}


