var products = [];
var productsForInventory = [];
Template.detailVisitInventorySeller.events({
  'click #cancelar': function () {
    event.preventDefault();
    Router.go('visitSellerInventory');    
  },
  'submit #reisterVisit': function(event, template) {
      event.preventDefault();
      registerVisitSeller(  
                            $('#notas').val(),
                            $('#claims').val()
                          );
  },
});

Template.detailVisitInventorySeller.onCreated(function () {
 

});

Template.detailVisitInventorySeller.rendered = function(){
  products = [];
  productsForInventory = [];
  setTimeout(function(){ 
      if ($("#status").val() == 1) {

      }else{
        $(".add-quantity").each(function(){
          Meteor.subscribe("inventorySelectForProductBuy",$(this).attr('id'));

          var restoreValueDiscountToInventory = Number($(this).attr('valueInventory')) + Number($(this).val());
          products.push(
                          {
                            product:$(this).attr('id'),
                            quantity:$(this).val(),
                            originalValuePreviousInventory: restoreValueDiscountToInventory ,
                          }
                        );
        });
        //console.log("products " + JSON.stringify(products));

        setTimeout(function(){ 
          var findVisit = VisitsSeller.findOne({_id:$("#id").val()});

          if (findVisit){

            for (var i=0; i < Object.keys(findVisit.resumeProductsInventory).length; i++){

              var findProductBuyInventory = Inventory.findOne({_id:findVisit.resumeProductsInventory[i].idInventory});
              var restoreInventoryForProduct = Number(findProductBuyInventory.actualQuantity) + Number(findVisit.resumeProductsInventory[i].quantity)

              productsForInventory.push(  
                                          {
                                            idInventory : findProductBuyInventory._id,
                                            quantityDiscountSaved:findVisit.resumeProductsInventory[i].quantity,
                                            restoreInventoryForProduct:restoreInventoryForProduct
                                          }
                                        );
            };

            //console.log("products Inventory" + JSON.stringify(productsForInventory)); 
          }
        }, 1500);
      }
      
  }, 1000);

  
}  



function registerVisitSeller(notes,claims){
  if (isNotEmpty(notas) === true){

    data = {
      _id:$('#id').val(),
      notes:notes,
      claims:claims,
      modifyAt: new Date(),
    }
    restoreInventoryCalcProduct(products);
    restoreValueInventoryForProducts(productsForInventory);
    Meteor.call('updateVisit',data, function(err, res){
      if (!err){
        Bert.alert( 'Se ha anulado la visita y las compras!', 'success' );
        Router.go('visitSellerInventory');   
      }
    });

  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}

function restoreInventoryCalcProduct(products){
  Meteor.call('updateInventoryCalcNullVisit',products, function(err, res){});
}

function restoreValueInventoryForProducts(productsForInventory){
  Meteor.call('updateInventoryCalcNullVisit',productsForInventory, function(err, res){});
}

