Template.inventoryVisitSeller.rendered = function(){

}

Template.inventoryVisitSeller.events({
	'click #register-visit': function(event){
        event.preventDefault();
         Router.go('register-visit-invt');
    },

    'click .details-visit-inventory': function(event){
        event.preventDefault();  
        Router.go('detailsVisitInventory',{_IdVisit:event.currentTarget.id});
    },

    
	
});

Template.inventoryVisitSeller.onCreated(function () {
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe('visits',Meteor.userId());

});

