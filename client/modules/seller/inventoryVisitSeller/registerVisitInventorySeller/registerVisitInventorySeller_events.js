var readyToRegisterInventory = 0;
var totalBuyClient = 0;
Session.set("discountInventoryforProduct","");

Template.registerVisitInventorySeller.events({
  'click #cancelar': function () {
    event.preventDefault();
    Router.go('dashboardSellerVisits');    
  },
  'submit #reisterVisit': function(event, template) {
      event.preventDefault();
    
      registerVisitSeller(
                            $('#name-client').val(),
                            $('#notas').val(),
                            $('#claims').val()
                            //$("#signature").jSignature('getData')
                          )
  },
  'click .add-client': function (event) {
    event.preventDefault();
    var id = event.currentTarget.id;
    searchInfo = Clients.findOne({_id:id});
    $('#name-client').attr('name', id);
    $('#name-client').val(searchInfo.name);

    $(".search-client").fadeOut("slow");

    $("#success-client-add").slideDown("slow")

    setTimeout(function(){ 
      $("#success-client-add").slideUp("slow") 
    }, 2000);
  },

  'click #cancel-add-client': function () {
    event.preventDefault();
    $(".search-client").fadeOut("slow"); 
  },
  'click #search-client': function () {
    event.preventDefault();
    $(".search-client").fadeIn();  
  },
  'click #notas': function () {
    event.preventDefault();
    $("#notas").get(0).setSelectionRange(0,0);
    $(".search-client").slideUp("slow");  
  },
  'click #claims': function () {
    event.preventDefault();
    $("#claims").get(0).setSelectionRange(0,0);
    $(".search-client").slideUp("slow");  
  },
  'click #orders': function () {
    event.preventDefault();
    $("#orders").get(0).setSelectionRange(0,0);
    $(".search-client").slideUp("slow");  
  },
  'click #clear-asignature': function () {  
    $("#signature").jSignature("reset")
    $(".signature-img").remove();
  },
  'click #img': function () {  
    var dataImg =$("#signature").jSignature('getData');
    $(".signature-img").remove();
    $("#img-show").append("<img class='signature-img' id='how-img-signature' src='"+dataImg+"'>");
  },
  'click #picture-visit': function () {
    event.preventDefault();
    var cameraOptions = {
        width: 800,
        height: 600,
    };
    MeteorCamera.getPicture(cameraOptions,function(err, res){
      if (!err){
        Session.set("picture" , res);
      };
    })
  },
  'change #categorie': function(event){
      event.preventDefault();
      Session.set("categorie",$("#categorie").val())
      Meteor.subscribe("productCategorie",$("#categorie").val());
      $("#add-products-table-inv").slideDown("slow");
  },

  'click #confirm-products': function () {
    event.preventDefault();
    $("#add-products-table-inv").slideUp("slow");
    checkProducts();
  },

  'click #visit-cont': function () {
    event.preventDefault();
    $(".visit-cont").remove();
    $("#continue-form").fadeIn("slow");
    checkProducts();
  },

  'click #recalculate': function () {
    event.preventDefault();
    
    recalculate();
  },

  'keyup .add-quantity': function (event) {
    event.preventDefault();
    //console.log("event.currentTarget.id " + event.currentTarget.id)
    Meteor.subscribe("product",event.currentTarget.id);
    Meteor.subscribe("inventoryCalcSelectProduct",event.currentTarget.id);

    setTimeout(function(){ 
      var find = calcInventory.findOne({product:event.currentTarget.id});
      var product = Products.findOne({_id:event.currentTarget.id});
      if (find){
        if (event.currentTarget.value > find.quantity ) {
          readyToRegisterInventory = 0;

          $(event.currentTarget).css({"background-color":"#e46050" , "color":"white" });
          $("#danger-limit-inv").slideDown("slow");

          setTimeout(function(){ 
            $("#danger-limit-inv").slideUp("slow") 
          }, 3000);

        }else{
          if (event.currentTarget.value > 0){

            readyToRegisterInventory = 1;

            $(event.currentTarget).css({"background-color":"white" , "color":"black" });
            var num = $("#total-quan-"+event.currentTarget.id).text().replace("$ ","");
            totalBuyClient =  Number(totalBuyClient) - Number(num);

            var calc = Number(event.currentTarget.value) * Number(product.price);
            
            totalBuyClient+= Number(calc);
            $("#total-quan-"+event.currentTarget.id).text("$ "+ localeString(calc));

          }else if(event.currentTarget.value == 0 ){
            $("#total-quan-"+event.currentTarget.id).text("$ 0");
          }
        }
        recalculate()
      }else{
        console.log("no find")
      }
    }, 200);
  },

  
});

Template.registerVisitInventorySeller.onCreated(function () {
    
    // Use this.subscribe inside onCreated callback
    Meteor.subscribe("userInfo",Meteor.userId());
    Meteor.setTimeout(function(){
      var user = Meteor.users.findOne({_id:Meteor.userId()});
      Session.set("enterprise" , user.profile.enterprise)
      Meteor.subscribe('allCategories',user.profile.enterprise);
      Meteor.subscribe('clients',user.profile.enterprise);
      delete Session.keys['picture'];
    }, 500);

    
});


Template.registerVisitInventorySeller.rendered = function(){
  
  setTimeout(function(){ 
    if ($("#client-program").val() != ""){

      $("#name-client").val($("#client-program-name").val()).attr("name", $("#client-program").val());
      $("#search-client").remove();
    };

    $("#signature").jSignature({color:"black",lineWidth:1})
  }, 500); 
}  

function recalculate(){
  var sum = 0;
  $(".total-price-prod").each(function(){
    var num = $(this).text().replace("$ ","");
    sum += Number(num.replace(".",""));

    totalBuyClient = sum;
    $("#total").text("$ " + localeString(totalBuyClient));
  });
  
}

function checkProducts(){
  $(".check-product").each(function(){
      if ($(this).is(':checked')) {

        if ($("#total-quan-" + $(this).attr('id')).length == 0) {
          var find = calcInventory.findOne({product:$(this).attr('id')});
          var product = Products.findOne({_id:$(this).attr('id')});

          var tbody = "<tr>"+
                          "<td>"+ product.name  +"</td>"+
                          "<td>"+ product.price +"</td>"+
                          "<td>"+ find.quantity +"</td>"+
                          '<td><input id="'+product._id+'" type="number" class="add-quantity vs-input-total"></td>'+
                          '<td><p id="total-quan-'+product._id+'" class="total-price-prod"></p></td>'
                      "</tr>";
          $("#products-buy tbody").append(tbody);
        }
      };
  });
}


function registerVisitSeller(client,notas,claims){
  var products = [];
  var sumTotalProducts= 0;

  if (isNotEmpty(client,notas) === true ){

    if (readyToRegisterInventory == 1) {
      var createAt = dateString();
      var lat =$("#lat").val();
      var lng =$("#lng").val();

      $(".add-quantity").each(function(){
          sumTotalProducts+=Number($(this).val());
          products.push(
                        {
                          product:$(this).attr('id'),
                          quantity:$(this).val(),
                        }
                      );
      });

      data = {
        createdById:Meteor.userId(),
        createdBy:Meteor.user().profile.name,
        enterprise:Meteor.user().profile.enterprise,
        idClient:$("#name-client").attr("name"),
        notes:notas,
        claims:claims,
        createdAt: createAt["stringDate"],
        //firma:firma,
        marker: {
                  lat: lat,
                  lng: lng,
                  infoWindowContent: "<div><h3>"+Meteor.user().profile.name+"</h3><br><p>"+ new Date() +"</p></div>"
                },
        photo:Session.get("picture"),
        products:products,
        totalQuantityProducts:sumTotalProducts,
        total:$("#total").text(),
        date:new Date(),
      };
      updateCalcInventory(products,"insert")
      //console.log(JSON.stringify(data))

      setTimeout(function(){ 
        if (Session.get("discountInventoryforProduct") != ""){
          
          data.resumeProductsInventory = Session.get("discountInventoryforProduct");

          if ($("#route").val() == ""){
            Meteor.call('saveDataVisit',data, function(err, res){
              if (err){
                Bert.alert( "Intenta de Nuevo!", 'danger' );
              }else{
                Bert.alert( 'Se ha registrado una nueva visita!', 'success' ); 
                history.back()
                //updateCalcInventory(products,"insert")  
              };
            });
          }else{
            var rute = $("#route").val();
            Meteor.call('saveDataVisitRute',data,rute,$("#client-program").val(), function(err, res){
              if (err){
                Bert.alert( "Intenta de Nuevo!", 'danger' );
              }else{
                Bert.alert( 'Se ha registrado una nueva visita!', 'success' ); 
                history.back()
                //updateCalcInventory(products,"insert")
              };

            });
          };
        }else{
          //console.log("Session.get(resumeInventoryPro) " + Session.get("resumeInventoryPro"))
        }
      }, 1000);
    }else{
        
    }
  }else{
    Bert.alert( "Rellena todos los Campos Correctamente!", 'danger' );
  };
}

localeString = function  (x, sep, grp) {
    var sx = (''+x).split('.'), s = '', i, j;
    sep || (sep = '.'); // default seperator
    grp || grp === 0 || (grp = 3); // default grouping
    i = sx[0].length;
    while (i > grp) {
        j = i - grp;
        s = sep + sx[0].slice(j, i) + s;
        i = j;
    }
    s = sx[0].slice(0, i) + s;
    sx[0] = s;
    return sx.join('.')
}


updateCalcInventory = function (products,form){

    if (form == "insert"){
      Meteor.call('updateCalcInventoryProduct', products,"","", function(error, result) {
          if (error) {  
            Bert.alert( "Intenta de Nuevo! " + error, 'danger' );
          }else{
            for (var i=0; i < Object.keys(products).length; i++){
              updateProductsInventory(products[i].product,products[i].quantity)
            }; 
          }
      }); 
    }else if(form == "edit"){

    };
}

//Funcion encargada de descontar por productos
updateProductsInventory = function (id,weight){

    var surplus = 0;
    var resumeInventoryPro = [];
    
    Meteor.call('findsProductsInventory', id, function(error, result) {
        if (!error) {

            //console.log("here discount " + JSON.stringify(result))
            $.each(result, function (index, value) {
                
                if (Number(value.actualQuantity) > 0 ) {

                    if (surplus == 0 ){

                        if ( Number(weight) > Number(value.actualQuantity)){

                            surplus += Number(weight) - Number(value.actualQuantity)
                            var rest = (Number(weight) - Number(surplus))
                            var restTwo = Number(value.actualQuantity) - Number(rest)

                            resumeInventoryPro.push({
                                                        idInventory:value._id,
                                                        quantity:rest,
                                                    })

                            Session.set("discountInventoryforProduct",resumeInventoryPro)
                            callUpdateStockProductInventory(value._id,restTwo)
                            //console.log("resumeInventoryPro " + JSON.stringify(resumeInventoryPro))
                        }else {

                            var rest = Number(value.actualQuantity) - Number(weight)
                            
                            resumeInventoryPro.push({
                                                        idInventory:value._id,
                                                        quantity:weight,
                                                    })

                            Session.set("discountInventoryforProduct",resumeInventoryPro)

                            callUpdateStockProductInventory(value._id,rest)
                            //console.log("resumeInventoryPro " + JSON.stringify(resumeInventoryPro))
                            //console.log("session data " + JSON.stringify(Session.get(session)))
                            //console.log("el peso que se envio es menor a " + value.actualQuantity + " del producto " + value._id)
                            //console.log("el producto " + value._id + " en stock quedo en 2 " + rest)
                            //console.log("esto es lo que se va restar " + weight + " a " + value.actualQuantity + " del producto " + value._id)
                            return false;
                        }   
                    }else{

                        //console.log("hay sobrante para quitar al siguiente producto " + surplus)

                        if (surplus > Number(value.actualQuantity)){

                            //console.log("el sobrante es mayor a la cantidad actual")
                            var lastSurplus = surplus ;
                            surplus = Number(surplus) - Number(value.actualQuantity); 
                            lastSurplus = lastSurplus - surplus;
                            var rest =  Number(value.actualQuantity)  - Number(lastSurplus)

                            resumeInventoryPro.push({
                                                        idInventory:value._id,
                                                        quantity:lastSurplus,
                                                    })
                            Session.set("discountInventoryforProduct",resumeInventoryPro)

                            callUpdateStockProductInventory(value._id,restTwo)

                        }else{
                            //console.log("el sobrante es menor a la cantidad actual ")
                            var rest = Number(value.actualQuantity) - Number(surplus);
                            resumeInventoryPro.push({
                                                        idInventory:value._id,
                                                        quantity:surplus,
                                                    })

                            Session.set("discountInventoryforProduct",resumeInventoryPro)

                            callUpdateStockProductInventory(value._id,rest)
                            //console.log("resumeInventoryPro " + JSON.stringify(resumeInventoryPro))
                            return false;
                        };

                    };
                }else{

                }
            });

            
        }
    });  
}

callUpdateStockProductInventory = function (id,value){
  //console.log("update product " + id)
  Meteor.call('updateInventoryProduct', id,value, function(error, result){
    if (!error){
      //Bert.alert( 'Se ha registrado una nueva visita!', 'success' ); 
      //history.back()
    }
  });
}