if (Meteor.isClient) {
  // counter starts at 0
  Template.registerVisitInventorySeller.helpers({
    loc: function () {
      // return 0, 0 if the location isn't ready
      return Geolocation.latLng() || { lat: 0, lng: 0 };
    },
    error:function(){
      return Geolocation.error
    },
  	showClients: function(object) {
       return Clients.find({enterprise:Session.get("enterprise")});
    },

    tableSettings : function () {
             return {
                        rowsPerPage: 6,
                        fields: [
                        	{ 
	                            key: '_id', 
	                            label: '' ,
	                            fn: function (_id, object) {
	                                var html =  '<button id="'+_id+'" class="add-client">Seleccionar</button>';
	                                return new Spacebars.SafeString(html);
	                            }
	                        },
                            { 
                                key: 'nit', 
                                label: 'Nit' ,

                            },
                            { 
                                key: 'name',
                                label: 'Nombre' 

                            },
                            { 
                                key: "address", 
                                label: 'Dirección' 
                            },
                           
                            
                           
                        ],
       
            };
    },

    tableSettingsProducts : function () {
      return {
                rowsPerPage: 6,
                fields: [
                          { 
                              key: '_id', 
                              label: '' ,
                              fn: function (_id, object) {
                                  var html =  '<input id="'+_id+'" class="check-product" type="checkbox" value=""/>';
                                  return new Spacebars.SafeString(html);
                              }
                          },
                          { 
                              key: 'name', 
                              label: 'Producto' ,

                          },
                          { 
                              key: 'price',
                              label: 'Precio' 
                          },
                          { 
                              key: '_id', 
                              label: 'Cantidad Disponible' ,
                              fn: function (_id, object) {
                                Meteor.subscribe("inventoryCalcSelectProduct",_id);
                                var find = calcInventory.findOne({product:_id});
                                if (find){
                                  return find.quantity;
                                }
                              }
                          },
                          /*{ 
                              key: '_id', 
                              label: 'Registrar' ,
                              fn: function (_id, object) {
                                var html =  '<input id="'+_id+'" type="number" class="add-quantity" >';
                                return new Spacebars.SafeString(html);
                              }
                          },
                          { 
                              key: '_id', 
                              label: 'Total Producto' ,
                              fn: function (_id, object) {
                                var html =  '<p id="total-quan-'+_id+'" ></p>';
                                return new Spacebars.SafeString(html);
                              }
                          },*/
                        ],

          };
    },

    picture:function(){
      return Session.get("picture")
    },
    showCategories: function() {
       return Categories.find({user:Session.get("enterprise")});
    },
    showProducts: function() {
       return Products.find({categorie:Session.get("categorie")});
    },
    showQuantityStock:function(_id){
      Meteor.subscribe("inventoryCalcSelectProduct",_id);
      var find = calcInventory.findOne({product:_id});
      if (find){
        return find.quantity;
      }
    }
  });
}


