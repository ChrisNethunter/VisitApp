if (Meteor.isClient) {
    Template.dashboard_menu.events({
    	'click #logout': function(event){
	        event.preventDefault();
	        Meteor.logout();
	    },
	    'click #map-sellers': function(event){
	        event.preventDefault();
	        Router.go('dashboardMap');
	    },
	    'click #sellers': function(event){
	        event.preventDefault();
	        Router.go('dashboardSellerAd');
	    },
	    'click #clients': function(event){
	        event.preventDefault();
	        Router.go('dashboardSellerClients');
	    },
	    'click #visits': function(event){
	        event.preventDefault();
	        //Router.go('dashboardSellerVisits');
	        Router.go('visitSellerInventory');
	        
	    },
	    'click #rutes': function(event){
	        event.preventDefault();
	        Router.go('dashboardSellerRutes');
	    },
	    'click #visits-seller-show-admin': function(event){
	        event.preventDefault();
	        Router.go('dashboardAdminShowVisitsSeller');
	    },
	    'click #rutesValidate': function(event){
	        event.preventDefault();
	        Router.go('programRutes');
	    },
	    'click #main-menu-toggle': function(event){
	        event.preventDefault();
	        if ($('body').hasClass("mmc")){
	        	$('body').removeClass( "mmc" )
	        	$('body').addClass('mme');
	        }else{
	        	$('body').removeClass( "mme" )
	        	$('body').addClass('mmc');
	        };
	    },
	    'click #categories': function(event){
	        event.preventDefault();
	        Router.go('categories');
	    },

	    'click #provider': function(event){
	        event.preventDefault();
	        Router.go('providers');
	    },

	    'click #products': function(event){
	        event.preventDefault();
	        Router.go('products');
	    },

	    'click #inventory': function(event){
	        event.preventDefault();
	        Router.go('inventory');
	    },
 
    });

    Template.dashboard_menu.rendered = function(){
    	dataloc()
		
	}
}

if (Meteor.isServer) {
  
}
