Router.configure({
	layoutTemplate: 'loginUser',  
});

Router.map(function(){
	this.route('home', {
		path: '/',
		layoutTemplate: 'loginUser',

	});	
	this.route('singUp', {
		path: '/singup',
		layoutTemplate: 'signUp',
	});	
	this.route('signIn', {
		path: '/signin',
		layoutTemplate: 'loginUser',
	});	

	/*ADMIN*/
	this.route('dashboard', {
		path: '/dashboard',
		layoutTemplate: 'dashboard',
	});

	//Inventory
	this.route('inventory', {
		path: '/inventory',
		layoutTemplate: 'inventory',
	});

	this.route('register-inventory', {
		path: '/addinventory',
		layoutTemplate: 'register_inventory',
	});

	this.route('update-inventory', {
		path: '/inventory/:_IdInventory',
		layoutTemplate: 'edit_inventory',
		waitOn	: function () {
			Meteor.subscribe('inventorySelect',this.params._IdInventory);
			var find = Inventory.findOne({_id:this.params._IdInventory});
			if (find){
				Meteor.subscribe('product',find.product);
				var find = Products.findOne({_id:find.product});
				if (find){
					Meteor.subscribe('provider',find.provider);
				}
			}
		},
		data:function(){
			return Inventory.findOne({_id:this.params._IdInventory});	
		},
	});

	//Products
	this.route('products', {
		path: '/products',
		layoutTemplate: 'products',
	});

	this.route('register-product', {
		path: '/addproduct',
		layoutTemplate: 'register_product',
	});

	this.route('update-product', {
		path: '/product/:_IdProduct',
		layoutTemplate: 'edit_product',
		waitOn	: function () {
			Meteor.subscribe('product',this.params._IdProduct);	
		},
		data:function(){
			return Products.findOne({_id:this.params._IdProduct});	
		},
	});

	//Categories
	this.route('categories', {
		path: '/categories',
		layoutTemplate: 'categorie',
	});

	this.route('register-categorie', {
		path: '/addcategorie',
		layoutTemplate: 'register_categorie',
	});

	this.route('update-categorie', {
		path: '/categorie/:_IdCategorie',
		layoutTemplate: 'edit_categorie',
		waitOn	: function () {
			Meteor.subscribe('categorie',this.params._IdCategorie);	
		},
		data:function(){
			return Categories.findOne({_id:this.params._IdCategorie});	
		},
	});
	//Provider
	this.route('providers', {
		path: '/providers',
		layoutTemplate: 'provider',
	});

	this.route('register-provider', {
		path: '/addprovider',
		layoutTemplate: 'register_provider',
	});


	this.route('update-provider', {
		path: '/provider/:_IdProvider',
		layoutTemplate: 'edit_provider',
		waitOn	: function () {
			Meteor.subscribe('provider',this.params._IdProvider);	
		},
		data:function(){
			return Providers.findOne({_id:this.params._IdProvider});	
		},
	});

	this.route('dashboardSellerAd', {
		path: '/seller',
		layoutTemplate: 'dashboard',
	});

	this.route('dashboardCreateSeller', {
		path: '/dashboard/create',
		layoutTemplate: 'registerSeller',

	});
	this.route('dashboardMap', {
		path: '/map',
		layoutTemplate: 'dashboardMap',
		waitOn	: function () {
			return [
					Meteor.subscribe('locations'),
					Meteor.subscribe('sellers'),
				   ]
		},

	});
	this.route('viewPosition', {
		path: '/dashboard/map/:_IdVisit',
		layoutTemplate: 'viewPosition',
		waitOn	: function () {
			Meteor.subscribe('dataVisit',this.params._IdVisit);	
		},
		data:function(){
			return  VisitsSeller.findOne({_id:this.params._IdVisit});
		},
	});
	this.route('viewPositionReal', {
		path: '/dashboard/mapr/:_IdSeller',
		layoutTemplate: 'viewPositionRealTime',
		waitOn	: function () {
			return [ Meteor.subscribe('dataVisitReal',this.params._IdSeller) , Meteor.subscribe('userInfo',this.params._IdSeller)
			];	
		},
		data:function(){
			return  Meteor.users.findOne({_id:this.params._IdSeller});
		},
	});


	this.route('updateSeller', {
		path: '/update/:_IdSeller',
		layoutTemplate: 'updateSeller',
		waitOn	: function () {
			Meteor.subscribe('sellers');	
		},
		data:function(){
			return  Meteor.users.findOne({_id:this.params._IdSeller});
		},
	});

	this.route('dashboardSellerRutes', {
		path: '/dashboard/rutes',
		layoutTemplate: 'rutes_seller',
	});

	this.route('programRutes', {
		path: '/dashboard/programRutes',
		layoutTemplate: 'rutesValidate',
	});

	this.route('dashboardAdminShowVisitsSeller', {
		path: '/dashboard/visits',
		layoutTemplate: 'dashboard_visit_sellers',

	});

	this.route('detailsVisitAdminShow', {
		path: '/detailsvisit/:_IdVisit',
		layoutTemplate: 'detailVisitSellerAdmin',
		waitOn	: function () {
			return [Meteor.subscribe('dataVisit',this.params._IdVisit)];	
		},
		data:function(){
			return VisitsSeller.findOne({_id:this.params._IdVisit});	 
		},
	});

	/*Sellers*/
	this.route('dashboardSellers', {
		path: '/sellers',
		layoutTemplate: 'dashboard_sellers',
		waitOn	: function () {
			return Meteor.subscribe('sellers');
		},

	});	
	this.route('dashboardMapSeller', {
		path: '/mapseller/:_user',
		layoutTemplate: 'dashboardMap',
		waitOn	: function () {
			return [
						Meteor.subscribe('locations'),
						Meteor.subscribe('sellers'),
				   ]
		},
	});
	this.route('dashboardSellerClients', {
		path: '/clients',
		layoutTemplate: 'dashboardSellerClients',
	});

	this.route('register-client', {
		path: '/register',
		layoutTemplate: 'registerClientSeller',
	});

	this.route('register-rute', {
		path: '/registerRute',
		layoutTemplate: 'createRute',
	});

	this.route('edit-client', {
		path: '/client/:_IdClient',
		layoutTemplate: 'editClientSeller',
		waitOn	: function () {
			Meteor.subscribe('client',this.params._IdClient);	
		},
		data:function(){
			return Clients.findOne({_id:this.params._IdClient});	
		},
	});

	this.route('edit-rute', {
		path: '/editRute/:_IdRute',
		layoutTemplate: 'editRute',
		waitOn	: function () {
			Meteor.subscribe('rute',this.params._IdRute);	
		},
		data:function(){
			return Rutes.findOne({_id:this.params._IdRute});	
			 
		},
	});

	this.route('detail-rute', {
		path: '/detailRute/:_IdRute',
		layoutTemplate: 'detailRute',
		waitOn	: function () {
			Meteor.subscribe('rute',this.params._IdRute);	
		},
		data:function(){
			return Rutes.findOne({_id:this.params._IdRute});	
			 
		},
	});				
	/*Visit Inventory*/
	this.route('visitSellerInventory', {
		path: '/visitinv',
		layoutTemplate: 'inventoryVisitSeller',
	});

	this.route('register-visit-invt', {
		path: '/registervisitin',
		layoutTemplate: 'registerVisitInventorySeller',
	});

	this.route('register-visit-par-inventory', {
		path: '/registerinv/client/:_IdCliente/:_Idroute',
		layoutTemplate: 'registerVisitInventorySeller',
		waitOn	: function () {
			Meteor.subscribe('client',this.params._IdCliente);	
		},
		data:function(){
			var data = {	
							client:Clients.findOne({_id:this.params._IdCliente}),
							rute:this.params._Idroute
						};
			return data
		},
	});

	this.route('detailsVisitInventory', {
		path: '/detailvisitinv/:_IdVisit',
		layoutTemplate: 'detailVisitInventorySeller',
		waitOn	: function () {
			return[Meteor.subscribe('visits',Meteor.userId())];	
		},
		data:function(){
			return VisitsSeller.findOne({_id:this.params._IdVisit});		 
		},
	});
	this.route('detailsVisitInventoryAdmin', {
		path: '/visitadmin/:_IdVisit',
		layoutTemplate: 'detailVisitInventorySellerAdmin',
		waitOn	: function () {
			return [Meteor.subscribe('dataVisit',Meteor.userId())];	
		},
		data:function(){
			return VisitsSeller.findOne({_id:this.params._IdVisit});		 
		},
	});	

	/*Visit Inventory*/


	/*this.route('dashboardSellerVisits', {
		path: '/visits',
		layoutTemplate: 'dashboardSellerVisits',
	});

	this.route('register-visit', {
		path: '/register/client',
		layoutTemplate: 'registerVisitSeller',
	});

	this.route('register-visit-par', {
		path: '/register/client/:_IdCliente/:_Idroute',
		layoutTemplate: 'registerVisitSeller',
		waitOn	: function () {
			Meteor.subscribe('client',this.params._IdCliente);	
		},
		data:function(){
			var data = {	
							client:Clients.findOne({_id:this.params._IdCliente}),
							rute:this.params._Idroute
						};
			return data
		},
	});

	this.route('detailsVisit', {
		path: '/detailvisit/:_IdVisit',
		layoutTemplate: 'detailVisitSeller',
		waitOn	: function () {
			Meteor.subscribe('visits',Meteor.userId());	
		},
		data:function(){
			return VisitsSeller.findOne({_id:this.params._IdVisit});		 
		},
	});*/

});

