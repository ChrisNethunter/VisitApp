Meteor.methods({
	initialD : function(){
		/*if (!Migrations.findOne({slug: 'users'})) {
			var users = [
			    {name:"Admin User",email:"social@vifuy.com", pass:"vifuyw4515991" , roles:['admin']},
			    {name:"Admin William ",email:"william@vifuy.com", pass:"w4515991" , roles:['admin']},
			    {name:"Admin Alejo ",email:"alejandro@vifuy.com" ,pass:"vifuy1234" , roles:['admin']},
				{name:"Admin Christian",email:"cristian@vifuy.com", pass:"vifuychobits" , roles:['admin']},
				{name:"Admin Alex",email:"alex@vifuy.com", pass:"" , pass:"vifuy1234" ,  roles:['admin']},
			];
			
			_.each(users, function (user) {
			  var id;

			  id = Accounts.createUser({
			    email: user.email,
			    password: user.pass,
			    profile: { name: user.name }
			  });

			  if (user.roles.length > 0) {
			    // Need _id of existing user record so this call must come 
			    // after `Accounts.createUser` or `Accounts.onCreate`
			    Roles.addUsersToRoles(id, user.roles);
			  }
			});

			Migrations.insert({slug: 'users'});
		}*/
	},
	'createUserSeller': function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		var user = Meteor.users.find({"profile.cc":data.cc}).count()
		if (user != 1){
			id = Accounts.createUser({
				email: data.email,
			    password: data.pass,
		    	profile:{ 
		    				name: data.name,
		    				cc: data.cc,
		    				createdBy: data.createdBy,
		    				phone: data.phone,
		    				status: data.status,
		    				request:"change-pass",
		    				emailseller:data.email,
		    				enterprise:data.enterprise,
		    				passProvisional: data.pass, 
		    			}
		    });
			return id
		}else{
			return 2
		};
	},

	'updateUserSeller': function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		update = Meteor.users.update({'_id':data._id},{$set:{"profile.name":data.name, "profile.phone":data.phone,"profile.emailseller":data.email,
					"profile.status":data.status
		}});

		if(update){
			return 1 
		}else{
			return 0
		};
		
	},

	'createUserWithRole': function(data) {
	    var userId;
	    var role = ['enterprise'];
	    var email = data.email;
	   	//console.log("data.email " + data.email)
	    var user = Meteor.users.find({"emails.address":email}).count()
	    if(user == 1){
	    	//console.log("encontro ya registrado")
	    	return 3
	    }else{
	    	//console.log("se va a registrar")
	    	Meteor.call('createUserNoRole', data, function(err, result) {
		      	if (err) {
		        	return err;
		      	}else{
		      		//console.log("result " + result)
		      		Roles.addUsersToRoles(result, role);
			      	// we wait for Meteor to create the user before sending an email
					Meteor.setTimeout(function() {
					    Accounts.sendVerificationEmail(result);
					}, 2 * 1000);
					//console.log("se registro")
		      	}	
			});
			return 1;
	    };    
	},

	'createUserNoRole': function(data) {
	    //Do server side validation
	    return Accounts.createUser({
	      email: data.email,
	      password: data.password,
	      profile: data.profile
	    });
	},


	'newPassword': function(password) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Accounts.resetPassword(Session.get('resetPassword'), password, function(err) {
            if (err) {
            	return 2
              	//console.log('We are sorry but something went wrong.');
            } else {
              	//console.log('Your password has been changed. Welcome back!');
              	Session.set('resetPassword', null);
              	return 1
            }
        });
	    
	},

	saveLocation: function(data) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		if(Meteor.userId()){
			userlocation.insert(data);
			return 1;
		}else{
			return err;
		}
  	},

  	setRoleToSellerUser : function(idSeller){
  		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Roles.addUsersToRoles(idSeller, ['seller']);
	},
  	setRoleToFreeUser : function(idU){
  		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		var id= Meteor.user();
		Roles.addUsersToRoles(id, ['freeUser']);
	},
	setRoleToPlatinumUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		var id= Meteor.user();
		Roles.addUsersToRoles(id, ['platinumUser']);
	},
	setRoleToGoldUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		var id= Meteor.user();
		Roles.addUsersToRoles(id, ['goldUser']);
	},
	updateRoleToFreeUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Roles.setUserRoles(idU, ['freeUser']);
	},
	updateRoleToPlatinumUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Roles.setUserRoles(idU, ['platinumUser']);
	},
	updateRoleToGoldUser : function(idU){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Roles.setUserRoles(idU, ['goldUser']);
	},
	updateRoleXToUser : function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Roles.setUserRoles(data.uId,data.rol);
		//return ('it\'s done boy');
	},

	sendEmailSeller:function(AdminEmail,to,pass){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Email.send({
		  from: AdminEmail,
		  to: to,
		  subject: "Cuenta Creada como vendedor",
		  text: "Has sido registrado/a como vendedor/a. Accede a " +  Meteor.absoluteUrl() +". Tu Usuario : Tu correo electronico y esta es tu contraseña " + pass
		});
	},

	saveDataClient: function(data) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		if(Meteor.userId()){
			Clients.insert(data);
			return 1;
		}else{
			return 0;
		}
  	},

  	updateDataClient: function(data,client) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }

		Clients.update({_id:data._id},{$set:{	name:data.name,
												nit:data.nit,
												phone:data.phone,
												address:data.address,
												status:data.status,
												modifyAt:new Date() 
						}} )
  	},

  	saveDataVisit: function(data) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		if(Meteor.userId()){
			VisitsSeller.insert(data);
			return 1;
		}else{
			return 0;
		}
  	},

  	saveDataVisitRute: function(data,rute,client) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    //Rutes.update({_id:rute,"clients.idClient":"JNgC3RLtJcjqLxRQ3" },{$set:{"clients.$.status": "visitado"}} )
		Rutes.update({_id:rute,"clients": { $elemMatch : { idClient : client } }},{$set:{"clients.$.status": "visitado"}} )
		VisitsSeller.insert(data);
	
  	},

  	updateRequestUser: function(user) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		update = Meteor.users.update({'_id':user},{$set:{"profile.request":"none"}});
  	},

  	'updateVisit': function(data){
  		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		VisitsSeller.update({'_id':data._id},
							{$set:{	
									"notes":data.notes, 
									"modifyAt":data.modifyAt,
									"canceled":1
							}});

	},

	realTimeLocIn : function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		//console.log("vendedor " + data.seller + "  logintud " + data.marker.lng + " latitud " + data.marker.lat)
		realTimeLoc.insert(data);
	},

	realTimeLocInRemove : function(seller){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }

		realTimeLoc.remove({seller:seller});
	},

	getPositionReal : function(sellerId){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    //console.log("")
		return realTimeLoc.findOne( {seller:sellerId},{sort: { $natural:-1 },limit:1} );
	},

	ruteInsert:function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Rutes.insert(data);
	},
	'ruteEdit': function(data,id){
  		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }

	 	Rutes.update({'_id':id},{$set:{	seller:data.seller, 
	 									modifyAt:data.modifyAt,
	 									rute:data.rute,
	 									clients:data.clients
	 								}});
	},
	searchClient:function(id){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
	    //console.log("client method " + id)
		return Clients.find({});
	},

	//*Categories*//

	categorieInsert:function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Categories.insert(data);
	},
	categorieEdit:function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Categories.update({'_id':data._id},{$set:{	name:data.name, 
													status:data.status,
													modifyAt:new Date()
												}});
	},

	//Providers	

	providerInsert:function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Providers.insert(data);
	},
	providerEdit:function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Providers.update({'_id':data._id},{$set:{	name:data.name,
													phone:data.phone,
													description:data.description, 
													status:data.status,
													"modifyAt":new Date()
												}});
	},

	//Products

	productInsert:function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		Products.insert(data);
	},

	productEdit: function(data) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }

		Products.update({_id:data._id},{$set:{	name:data.name,
												categorie:data.categorie,
												provider:data.provider,
												price:data.price,
												description:data.description,
												status:data.status,
												modifyAt:new Date() 
						}} )
  	},

  	//Inventory

  	inventoryInsert:function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		return Inventory.insert(data);
	},

	inventoryEdit: function(data) {
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }

		Inventory.update({_id:data._id},{$set:{	
													product:data.product,
													priceBuy:data.priceBuy,
													quantity:data.quantity,
													actualQuantity:data.actualQuantity,
													description:data.description,
													modifyAt:new Date() 
												}});
  	},

	'validateExistInventory':function(product){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }else{
	    	return calcInventory.find({product:product}).count()
	    }
	},

	initCalcInventory:function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }
		return calcInventory.insert(data);
	},

	'updateCalcInventory':function(data){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }else{
	    	var findData = calcInventory.findOne({product:data.product})
	    	var sum = Number(findData.quantity) + Number(data.quantity)

    		calcInventory.update({_id:findData._id},{$set:{quantity:Number(sum),modifyAt:data.modifyAt}});
	    }
	},

	'updateCalcInventoryProduct':function(products){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }else{
	    	// For all products to need recalculate on inventory
	    	for (var i=0; i < Object.keys(products).length; i++){
	    		//Find Registe for product on calcInventory for rest on stock with quantity product
	    		var findData = calcInventory.findOne({product:products[i].product})
	    		if (findData){
	    			//Rest for new calc on stock for product
	    			var calcRest = Number(findData.quantity - products[i].quantity);
	    			//Update CalcInventory
	    			calcInventory.update({_id:findData._id},{$set:{quantity:Number(calcRest),modifyAt:new Date() }});
	    		}
	        };  
	    }
	},

	updateInventoryCalc:function(product,newValueCalc){

		var findData = calcInventory.findOne({product:product})

		calcInventory.update({_id:findData._id},{$set:{"quantity":Number(newValueCalc),"modifyAt":new Date()}});

	},

	'sumInventory':function(product){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }else{
	    	//console.log(product)
	        var quantityActual = calcInventory.findOne({product:product})

	        if (quantityActual){
	        	return quantityActual.quantity
	        }; 
	    };
	},


	// Busqueda de productos para descontar stock por producto
	'findsProductsInventory':function(id){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }else{
	        return Inventory.find({product:id},{fields:{"actualQuantity":1,"quantity":1}}).fetch()
	    };
	},


	// Aqui se modifica para actualizar la cantidad en stock por producto
	'updateInventoryProduct':function(id,actualQuantity){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }else{
	    	if (Number(actualQuantity) > 0){
	    		Inventory.update({_id:id},{$set:{actualQuantity:actualQuantity,modifyAt:new Date()}});
	    	}else{
	    		Inventory.update({_id:id},{$set:{actualQuantity:actualQuantity,modifyAt:new Date(),"status":"inactivo"}});
	    	}; 
	    };
	},

	//Restaurando Valores de Compra en el inventario cuando se anula la visita

	updateInventoryCalcNullVisit:function(products){
		if (! Meteor.userId()) {
	      	throw new Meteor.Error("not-authorized");
	    }else{
			for (var i=0; i < Object.keys(products).length; i++){

				var findData = calcInventory.findOne({product:products[i].product});
				var newValueCalc = Number(findData.quantity) + Number(products[i].quantity);

				calcInventory.update({_id:findData._id},{$set:{"quantity":Number(newValueCalc),"modifyAt":new Date()}});
	         
	        };
	    }
	},

	'updateInventoryProductNullVisit':function(productsInventory){
		if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	    }else{
	    	for (var i=0; i < Object.keys(productsInventory).length; i++){
	    		var findData = Inventory.findOne({_id:productsInventory[i].idInventory});
	    		var newValueCalc = Number(findData.actualQuantity) + Number(products[i].quantity);
				Inventory.update({_id:findData._id},{$set:{"actualQuantity":Number(newValueCalc),"modifyAt":new Date()}});
	    	}
	    };
	},

	//Restaurando Valores de Compra en el inventario cuando se anula la visita

	//Inventory
})