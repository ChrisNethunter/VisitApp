Accounts.config({
  sendVerificationEmail: true,
  forbidClientAccountCreation: false,
});

process.env.MAIL_URL="smtp://social%40vifuy.com:vifuyvifuy1234@smtp.gmail.com:465/"; 
Meteor.startup(function () {
	Meteor.call('initialD');

});

Meteor.publish('users', function(){
  	return Meteor.users.find();
});

Meteor.publish('userInfo', function(user){
  	return Meteor.users.find({_id:user});
});

Meteor.publish('locations', function(){
  	return userlocation.find();
});

Meteor.publish('sellers', function(enterprise){
  	return Meteor.users.find({"profile.enterprise":enterprise,roles:"seller"});
});

Meteor.publish('visitsAdminShow', function(enterprise){
  	return VisitsSeller.find({enterprise:enterprise},{_id:"",createdById:"",createdBy:"",idClient:"",nameClient:"",address:"",createdAt:""});
});

Meteor.publish('clients-detail-visit', function(user){
  	return Clients.find({enterprise:user});
});

Meteor.publish('rutes', function(enterprise){
    return Rutes.find({user:enterprise});
});

Meteor.publish('rute', function(rute){
    return Rutes.find({_id:rute});
});

Meteor.publish('ruteSeller', function(sellerId){
    return Rutes.find({seller:sellerId});
});
/* Admin Seller*/

Meteor.publish('visits', function(user){
  	return VisitsSeller.find({createdById:user},{_id:"",createdById:"",createdBy:"",idClient:"",nameClient:"",address:"",createdAt:""});
});

Meteor.publish('dataVisit', function(id){
  	return VisitsSeller.find({_id:id});
});

Meteor.publish('dataVisitReal', function(id){
  return realTimeLoc.find({seller:id});
});

Meteor.publish('clients', function(enterprise){
  	return Clients.find({enterprise:enterprise});
});

Meteor.publish('client', function(id){

    return Clients.find({_id:id});
});

Meteor.publish('allClients', function(){
    return Clients.find({});
});

//*Inventory*//

//Categories
Meteor.publish('categories', function(user){
    return Categories.find({user:user});
});

Meteor.publish('categorie', function(id){
    return Categories.find({_id:id});
});

Meteor.publish('allCategories', function(id){
    return Categories.find({});
});

//Providers

Meteor.publish('providers', function(user){
    return Providers.find({user:user});
});

Meteor.publish('provider', function(id){
    return Providers.find({_id:id});
});

Meteor.publish('providersAlls', function(id){
    return Providers.find({});
});


//Products 

Meteor.publish('products', function(user){
    return Products.find({user:user});
});

Meteor.publish('product', function(id){
    return Products.find({_id:id});
});

Meteor.publish('productCategorie', function(id){
    return Products.find({categorie:id});
});

//Inventory
Meteor.publish('inventory', function(user){
    return Inventory.find({user:user});
});

Meteor.publish('inventorySelect', function(id){
    return Inventory.find({_id:id});
});

Meteor.publish('inventorySelectForProductBuy', function(id){
    return Inventory.find({product:id});
});

Meteor.publish('inventoryCalcSelectProduct', function(id){
    return calcInventory.find({product:id});
});




//*Inventory*//



